﻿using UnityEngine;
using System.Collections;

public class BedroomCutScene : MonoBehaviour {

    public FirstPersonDrifter player;


    Animator anim;


    int leftTurn = Animator.StringToHash("leftTurn");
    int rightTurn = Animator.StringToHash("rightTurn");
    int rightStrafe = Animator.StringToHash("rightStrafeWalk");
    int leftStrafe = Animator.StringToHash("leftStrafeWalk");
    int walk = Animator.StringToHash("walk");
    int idle = Animator.StringToHash("idle");

    //triggers
    int walkTrigger = Animator.StringToHash("walkTrigger");
    int idleTrigger = Animator.StringToHash("idleTrigger");
    int leftStrafeTrigger = Animator.StringToHash("leftStrafeTrigger");
    int rightStrafeTrigger = Animator.StringToHash("rightStrafeTrigger");
    int rightTurnTrigger = Animator.StringToHash("rightTurnTrigger");
    int leftTurnTrigger = Animator.StringToHash("leftTurnTrigger");

    public bool DoneAnimating = false;
    public bool StartAnimating = false;
    public float turnTime = 1.0f;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();


	}
	
	// Update is called once per frame
	void Update () {

        //if(triggerBox == null)
        //{
        //    triggerBox = (GameObject.Find("BedroomTrigger") as GameObject).GetComponent<ContactedPlayer>();
        //}

	    if(StartAnimating && !DoneAnimating)
        {
           // player.DisablePlayerControl();
            turnTime -= Time.deltaTime;
            transform.Rotate(new Vector3(0, -200.0f * Time.deltaTime, 0));
            if (turnTime <= 0.0f)
            {
                transform.rotation = Quaternion.Euler(new Vector3(0, 90.0f, 0));
                //anim.SetFloat("Speed", 1.0f);
                DoneAnimating = true;
                StartAnimating = false;
            }
        }
	}

    private bool _sing = false;

    public bool Sing
    {
        get { return _sing; }
        set { _sing = value; 
            if (_sing) 
            { 
                audio.Play(); 
            } 
            else 
            { 
                audio.Stop();
            } 
        }
    }
}
