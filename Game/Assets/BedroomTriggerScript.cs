﻿using UnityEngine;
using System.Collections;

public class BedroomTriggerScript : MonoBehaviour {

	// Use this for initialization
    public bool ContactedPlayer = false; 


	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            ContactedPlayer = true;
        }
    }
}
