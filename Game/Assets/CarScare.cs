﻿using UnityEngine;
using System.Collections;

public class CarScare : MonoBehaviour
{
	public GameObject car;
	public GameObject nodes;
	public GameObject lights;
	public AudioClip Starting, Run, Crash;
	public float speed = 1;
	private Transform[] theRealNodes;
	private int currentNode = 1;
	private float lerp = 0;
	private bool activated = false;
	// Use this for initialization
	void Start ()
	{
		theRealNodes = nodes.transform.GetComponentsInChildren<Transform>();
		if ( theRealNodes.Length > 0 && theRealNodes[0].gameObject.Equals( nodes ) ) ++currentNode;
		if ( lights ) lights.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(activated)
		{
			if ( currentNode < 1 || currentNode >= theRealNodes.Length ) return;
			if ( car.audio && Starting && car.audio.clip && car.audio.clip.Equals( Starting ) && car.audio.isPlaying ) return;
			if ( car.audio && Run && car.audio.clip && car.audio.clip.Equals( Starting ) && !car.audio.isPlaying )
			{
				car.audio.clip = Run;
				car.audio.loop = true;
				car.audio.Play();
			}
			lerp += Time.deltaTime * speed;
			bool over = lerp >= 1;
			if ( over ) lerp = 1;
			car.transform.position = Vector3.Lerp( theRealNodes[currentNode - 1].transform.position, theRealNodes[currentNode].transform.position, lerp );
			car.transform.rotation = Quaternion.Lerp( theRealNodes[currentNode - 1].transform.rotation, theRealNodes[currentNode].transform.rotation, lerp );
			if ( over )
			{
				++currentNode;
				lerp = 0;
			}
			if ( currentNode >= theRealNodes.Length && car.audio && Crash )
			{
				car.audio.Stop();
				car.audio.clip = Crash;
				car.audio.loop = false;
				car.audio.Play();
				if ( lights ) lights.SetActive( false );
			}
		}
	}
	void OnTriggerEnter(Collider other)
	{
		if ( activated || ( currentNode < 1 || currentNode >= theRealNodes.Length ) ) return;
		if ( other.gameObject.tag == "Player" )
		{
			activated = true;
			if ( car.audio && Starting )
			{
				car.audio.clip = Starting;
				car.audio.loop = false;
				car.audio.Play();
			}
			if ( lights ) lights.SetActive( true );
		}
	}
}
