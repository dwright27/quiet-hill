﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CombinationPuzzleManager : MonoBehaviour
{

    //reward is a key, when the puzzle is solved move the key to given position, should only move the key once
    //puzzles cannot be reset
    public GameObject puzzleReward;
    public GameObject[] turnNodes;
    public GameObject[] puzzleButtons;
    public Vector3 rewardDropPosition;
    public AudioClip keyDrop;

    private bool solved = false;

    public bool isSolved()
    {
        return solved;
    }

    void Update()
    {
        if (!solved)
        {
            //check if all of the nodes are in their correct position
            solved = true;
            for (int i = 0; i < turnNodes.Length; i++)
            {
                Component turningComponent = turnNodes[i].gameObject.GetComponent<TurnNode>();
                bool correct = ((TurnNode)turningComponent).checkStatus();
                if (!correct)
                {
                    solved = false;
                    break;
                }
            }

            if (solved)
            {
                //disable Turn Nodes
                for (int i = 0; i < turnNodes.Length; i++)
                {
                    //turnNode triggers now shouldn't turn
                    turnNodes[i].gameObject.GetComponent<TurnNode>().setActiveState(false);
                    //nodes[i].transform.GetChild(0).gameObject.transform.Translate(new Vector3(0, 10, 0));
                }

                //disable button glow
                for (int i = 0; i < puzzleButtons.Length; i++)
                {
                    puzzleButtons[i].GetComponent<Glow>().enabled = false;
                }

                //drop key
                if (puzzleReward != null)
                {
                    puzzleReward.transform.position = rewardDropPosition;
                    audio.clip = keyDrop;
                    audio.Play();
                }
            }
        }
    }
}
