﻿using UnityEngine;
using System.Collections;

public class CopyLight : MonoBehaviour {

    private Light light;
	// Use this for initialization
	void Start () {
        light = transform.parent.GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<Light>().enabled = light.enabled;
	}
}