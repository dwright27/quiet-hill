﻿using UnityEngine;
using System.Collections;

public class CrackCutscene : Door
{
	public AudioSource Stabs, Screams, Laughing;
	public GameObject Girl;
	public GameObject leCam;
	private GameObject player;
	private bool investigated = false, animating = false;

	// Update is called once per frame
	public override void Update ()
	{
		base.Update();
		if ( !investigated || !animating ) return;
		if ( Stabs.isPlaying ) return;
		if(!Girl.activeSelf)
		{
			Girl.SetActive( true );
			leCam.GetComponentInChildren<Camera>().fieldOfView = 20;
			Stabs.Stop();
			Screams.Stop();
			Laughing.Stop();
		}
		
		if(!Girl.GetComponentInChildren<AudioSource>().isPlaying)
		{
			Girl.SetActive( false );
			Slam();
			animating = false;
			leCam.SetActive( false );
			player.SetActive( true );
			GetComponent<Glow>().HaloEnabled = true;
			GetComponent<Glow>().enabled = true;
		}
	}

	public override void Investigate( UnityEngine.GameObject player = null )
	{
		if ( !investigated )
		{
			investigated = true;
			animating = true;
			Open();
			Stabs.Play();
			Screams.Play();
			Laughing.Play();
			leCam.SetActive( true );
			this.player = player;
			player.SetActive( false );
			GetComponent<Glow>().HaloEnabled = false;
			GetComponent<Glow>().enabled = false;
		}
		else
		{
			base.Investigate( player );
		}
	}
}
