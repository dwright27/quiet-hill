﻿using UnityEngine;
using System.Collections;


//attach this to the trigger which starts a scare
//or attach this to an investigatible 
//
public class zombieTriggerScript : MonoBehaviour
{
    public GameObject zombie;
    public AudioClip zombieIntro;
    public float animationStartDelay = 0;
    private bool scareStarted = false;
    private float timer;

    public AudioSource zombieAudio;

    public void Update()
    {
        if (!scareStarted)
        {
            Component[] allComps = GetComponents<Component>();
            for (int i = 0; i < allComps.Length; i++)
            {
                //this scareStartTrigger component is attached to an investigatable object which triggers a scary when investigated
                if (allComps[i] is Door)
                {
                    if (((Door)allComps[i]).isOpen())
                    {
                        triggerInvestigateIntro();
                    }
                }
            }
        }
        else {
            timer += Time.deltaTime;
            if (timer > animationStartDelay)
            {
                if(zombie)zombie.GetComponent<Animator>().SetBool("introduced", true);
            }
        }
    }

    public void triggerInvestigateIntro()
    {
        zombieAudio.clip = zombieIntro;
        zombieAudio.Play();
        scareStarted = true;
    }
}