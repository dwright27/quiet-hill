﻿using UnityEngine;
using System.Collections;

public class DestroyScript : MonoBehaviour {


    public float timeAlive = 5.0f;


	// Use this for initialization
	void Start () {
        Destroy(gameObject, timeAlive);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
