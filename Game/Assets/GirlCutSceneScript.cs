﻿using UnityEngine;
using System.Collections;

public class GirlCutSceneScript : MonoBehaviour {


    Animator anim;


    int leftTurn = Animator.StringToHash("leftTurn");
    int rightTurn = Animator.StringToHash("rightTurn");
    int rightStrafe= Animator.StringToHash("rightStrafeWalk");
    int leftStrafe = Animator.StringToHash("leftStrafeWalk");
    int walk = Animator.StringToHash("walk");
    int idle = Animator.StringToHash("idle");


    public float standTime = 1.0f;
    public float turnTime = 0.5f;
    public float walkTime = 4.0f;
    public float speed = 15.0f;
    public float rotationSpeed = 200.0f;
    //triggers
    int walkTrigger = Animator.StringToHash("walkTrigger");
    int idleTrigger = Animator.StringToHash("idleTrigger");
    int leftStrafeTrigger = Animator.StringToHash("leftStrafeTrigger");
    int rightStrafeTrigger = Animator.StringToHash("rightStrafeTrigger");
    int rightTurnTrigger = Animator.StringToHash("rightTurnTrigger");
    int leftTurnTrigger = Animator.StringToHash("leftTurnTrigger");


    public bool StartAnimating = false;


	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
       // anim.SetTrigger(id);
	}
	
	// Update is called once per frame
	void Update () {
       // float move = Input.GetAxis("Vertical");
       // anim.SetFloat("Speed", move);
        if (StartAnimating)
        {
            if (standTime >= 0.0f)
            {
                standTime -= Time.deltaTime;
                if (standTime <= 0.0f)
                {
                    anim.SetTrigger(leftTurnTrigger);
                }
            }
            else if (turnTime >= 0.0f)
            {
                turnTime -= Time.deltaTime;
                transform.Rotate(new Vector3(0, -rotationSpeed * Time.deltaTime, 0));
                if (turnTime <= 0.0f)
                {
                    transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                    anim.SetFloat("Speed", 1.0f);
                }
            }
            else if (walkTime >= 0.0f)
            {
                walkTime -= Time.deltaTime;
                transform.Translate((Quaternion.Euler(0.0f, -90.0f, 0.0f) * -transform.forward) * speed * Time.deltaTime);
                if (walkTime <= 0.0f)
                {
                    anim.SetFloat("Speed", 0.0f);
                    anim.SetTrigger(idleTrigger);
                    Destroy(gameObject);
                }
            }
        }
	}
}
