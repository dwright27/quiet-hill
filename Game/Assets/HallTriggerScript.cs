﻿using UnityEngine;
using System.Collections;

public class HallTriggerScript : MonoBehaviour {


    public bool ContactedPlayer = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")
        {
            ContactedPlayer = true;
        }
    }
}
