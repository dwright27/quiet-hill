﻿using UnityEngine;
using System.Collections;

public class InvestigatiblePuzzleButton : Investigatable {

    public GameObject puzzleManager;
    public GameObject turnNode;
    public float buttonPushDistance = 1;
    private bool glowEnabled;

    private bool isButtonMoving;
    private Vector3 button_neutral;
    private Vector3 button_pushed;
    private string buttonName = "Button_Push";
    private float beta;
    private bool down;

    void Start () {
        isButtonMoving = false;
        button_neutral = turnNode.transform.localPosition;
        button_pushed = new Vector3(button_neutral.x, button_neutral.y - buttonPushDistance, button_neutral.z);
        beta = 0;
        down = false;
        //button glows as long as the puzzle is unsolved
        glowEnabled = true;
        //glowEnabled = !puzzleManager.GetComponent<CombinationPuzzleManager>().solved;
	}
	
	void Update () {
        if (isButtonMoving)
        {
            turnNode.transform.localPosition = (down) ? Vector3.Lerp(button_pushed, button_neutral, beta) : Vector3.Lerp(button_neutral, button_pushed, beta);
            Vector3 currPosition = turnNode.transform.localPosition;

            beta ++;
            if (beta >= 1.0f) 
            {
                if(currPosition.y - button_neutral.y > float.Epsilon ||
                    currPosition.y - button_pushed.y > float.Epsilon)
                {
                    beta = 0;
                    down = !down;
                    isButtonMoving = down;
                }
            };
        }
	}

    //change state of all attached turnNodes when used
    public override void Investigate(GameObject player = null)
    {
        turnNode.GetComponent<TurnNode>().changeState();
        isButtonMoving = true;
    }

    public override InvestigatableEnum getInvestigateState(GameObject player = null)
    {
        //player.GetComponent<FirstPersonDrifter>().ActionHint.text = "Push";
        return InvestigatableEnum.Use;
    }

	public override bool canInvestigate( GameObject player = null )
    {
        if (puzzleManager.GetComponent<CombinationPuzzleManager>().isSolved())
        {
            glowEnabled = false;
            return glowEnabled;
        }

        return true;
    }
}
