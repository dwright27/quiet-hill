﻿using UnityEngine;
using System.Collections;

public class LockDigitScript : MonoBehaviour {


    public PentagramPuzzle puzzle;
    bool visible = false;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(puzzle.Solved && !visible)
        {
            Color color = renderer.material.color;
            color.a += 0.25f * Time.deltaTime;
            renderer.material.color = color;
            if(color.a >= 1.0f)
            {
                visible = true;
                puzzle.StopSolvingPentaPuzzle();
            }
        }
	}
}
