﻿using UnityEngine;
using System.Collections;

public class GameOverScreen : MonoBehaviour {

	enum MenuEnum { None, Play, Exit }
	private MenuEnum option = MenuEnum.None;
	private AudioSource[] audios;
	public Collider Play, Exit;
	public float fadeSpeed = 1;
	private float lerp = 0;
	// Use this for initialization
	void Start()
	{
		audios = GameObject.FindObjectsOfType<AudioSource>();
		Screen.lockCursor = false;
	}

	// Update is called once per frame
	void Update()
	{
		//transform.parent.Rotate( Vector3.up, Time.deltaTime * 5 );
		if ( Input.GetMouseButtonUp( 0 ) && option.Equals( MenuEnum.None ) )
		{
			RaycastHit hitInfo;
			var ray = Camera.main.ScreenPointToRay( Input.mousePosition );
			if ( Physics.Raycast( ray, out hitInfo, 20 ) )
			{
				if ( hitInfo.collider.Equals( Play ) )
				{

					option = MenuEnum.Play;
					collider.enabled = true;

				}
				else if ( hitInfo.collider.Equals( Exit ) )
				{
					collider.enabled = true;
					option = MenuEnum.Exit;
				}
			}
		}
		if ( !option.Equals( MenuEnum.None ) )
		{
			lerp -= Time.deltaTime * fadeSpeed;
			if ( lerp < 0 ) lerp = 0;
			Color color = GetComponent<Renderer>().material.color;
			Color newColor = new Color( color.r, color.g, color.b, color.a );
			newColor.a = Mathf.Lerp( 1, 0, lerp );
			GetComponent<Renderer>().material.color = newColor;
			foreach ( AudioSource i in audios )
			{
				i.volume = Mathf.Lerp( 0, 1, lerp );
			}
			if ( lerp == 0 )
			{
				if ( option.Equals( MenuEnum.Play ) ) Application.LoadLevel( 1 );
				else if ( option.Equals( MenuEnum.Exit ) ) Application.LoadLevel(0);
			}
		}
		else if ( option.Equals( MenuEnum.None ) && lerp < 1 )
		{
			lerp += Time.deltaTime * fadeSpeed;
			if ( lerp > 1 ) lerp = 1;
			Color color = GetComponent<Renderer>().material.color;
			Color newColor = new Color( color.r, color.g, color.b, color.a );
			newColor.a = Mathf.Lerp( 1, 0, lerp );
			GetComponent<Renderer>().material.color = newColor;
			foreach ( AudioSource i in audios )
			{
				i.volume = Mathf.Lerp( 0, 1, lerp );
			}
		}
	}
}
