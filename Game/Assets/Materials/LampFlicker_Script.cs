﻿using UnityEngine;
using System.Collections;

public class LampFlicker_Script : MonoBehaviour {

    private Light light;
    public float maxOnTime = 2;
    public float maxDelayTime = 3;

    private float currentOnTime = 0;
    private float currentDelayTime = 0;
    private float timer = 0;

	// Use this for initialization
	void Start () {
        light = GetComponent<Light>();
        if (!light)
            Debug.LogError("No Light component attached");
        GenerateRandom();
        light.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        if ((light.enabled && timer > currentOnTime) || (!light.enabled && timer > currentDelayTime))
        {
            ToggleLight();
            if(!light.enabled)
                GenerateRandom();
        }
	}

    void GenerateRandom()
    {
        currentOnTime = Random.Range(0, maxOnTime);
        currentDelayTime = Random.Range(0, maxDelayTime);
    }

    void ToggleLight()
    {
        light.enabled = !light.enabled;
        timer = 0;
    }
}
