﻿using UnityEngine;
using System.Collections;

public class MoveTowardPlayer : MonoBehaviour {

	// Use this for initialization
    public GameObject player;
    public float timeLimit;
    private Vector3 direction;
    private float timeAlive;


	void Start () {
        direction = (player.transform.position - transform.position).normalized;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(direction);
        timeAlive += Time.deltaTime;
        if(timeAlive > timeLimit)
        {
            Destroy(this);
        }
	}
}
