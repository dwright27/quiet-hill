﻿using UnityEngine;
using System.Collections;

public class NotificationTrigger_Script : MonoBehaviour {

    public float notificationTime = 15.0f;
    public GameObject text;
    float timer = -1.0f;
	// Use this for initialization
	void Start () {
        text.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (timer > -1.0f)
        {
            timer += Time.deltaTime;
            if (timer > notificationTime)
            {
                timer = -1.0f;
                text.SetActive(false);
                //transform.active = false;
                Destroy(gameObject);
            }
        }
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            text.SetActive(true);
            timer = 0;
        }
    }
}
