﻿using UnityEngine;
using System.Collections;

public class PanelButton : Investigatable {

	// Use this for initialization

    public delegate void ButtonPressed(int value);
    public ButtonPressed ButtonPress;
   

    public int value;
    public PasswordPanelScript panel;

    private float colorChangeTimer = 0;
    private float colorTimeLimit = 0.5f;
    public Color startBackgroundColor = Color.gray;

	void Start () {
        panel.AddButton(this);
    }
	
	// Update is called once per frame
	void Update () {
        colorChangeTimer += Time.deltaTime;

        if (gameObject.renderer.material.color != startBackgroundColor && colorChangeTimer > colorTimeLimit)
        {
            gameObject.renderer.material.color = startBackgroundColor;
        }
        else if(gameObject.renderer.material.color == startBackgroundColor)
        {
            gameObject.renderer.material.color = panel.GetColorFeedback();
            if (gameObject.renderer.material.color == Color.green) 
            {
                colorTimeLimit = 10000; 
            }
        }
	}

    public void PressButton()
    {
        if(ButtonPress != null)
        {
            //when pressed flash green
            ButtonPress(value);
            gameObject.renderer.material.color = Color.green;
            colorChangeTimer = 0;
            audio.Play();
        }
    }

    public override void Investigate(GameObject player = null)
    {
        PressButton();
    }

    public override InvestigatableEnum getInvestigateState(GameObject player = null)
    {
        return InvestigatableEnum.Use;
    }

	public override bool canInvestigate( GameObject player = null )
    {
        
        return true;
    }
}
