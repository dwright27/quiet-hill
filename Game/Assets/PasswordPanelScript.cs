﻿using UnityEngine;
using System.Collections;

public class PasswordPanelScript : DoorUnlockChecker {

    public bool debug = false;
    public delegate void ButtonPressed(int value);
    public static ButtonPressed PanelButtonPressed;
    public PentagramPuzzle puzzle1;
    public ScalePuzzle puzzle2;

    public bool puzzle1Complete = false;
    public bool puzzle2Complete = false;
    public string password = "4368";
    private string passwordGuess = "";
    public Door door;
    private bool unlocked = false;
    private const  float DELAY = 0.5f;
    private float delayLeft = 0.0f;

    //used for determining if the code was just input
    private bool guessed =  false;
    private float colorTimer = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if(delayLeft >= 0.0f)
        {
            delayLeft -= Time.deltaTime;
        }

        if (guessed)
        {
            colorTimer += Time.deltaTime;
            if (colorTimer > DELAY)
            {
                colorTimer = 0;
                guessed = false;
            }
        }
	}

    public override bool canOpen(GameObject player = null)
    {
        return unlocked;
    }


    private void PressButton(int value)
    {
        if (!unlocked && delayLeft <= 0.0f)
        {
            if (puzzle1.Solved && !puzzle1Complete)
            {
                puzzle1Complete = true;
            }

            if (puzzle2.Solved() && !puzzle2Complete)
            {
                puzzle2Complete = true;
            }
            Debug.Log(value);
            passwordGuess += value;
            if (passwordGuess.Length == password.Length)
            {
                guessed = true;
                Debug.Log(passwordGuess);

                //toggle commented/uncommented lines for debug
                if (((puzzle1Complete && puzzle2Complete) || debug) && password.Equals(passwordGuess))
                {
                    door.Open();
                    unlocked = true;
                }
                else
                {
                    passwordGuess = string.Empty;
                    audio.Play();
                }
            }

            delayLeft = DELAY;
        }

    }

    public void AddButton(PanelButton button)
    {
        button.ButtonPress += PressButton;
    }

    //returns true if the button should flash red
    public Color GetColorFeedback()
    {
        //when the buttons are pressed they should change colors for an amount of time

        //password was reset to empty; flash red
        if (!unlocked && guessed)
        {
            return Color.red;
        }
        else if (unlocked)
        {
            return Color.green;
        }
        else {
            return Color.gray;
        }
    }

    public bool IsSolved()
    {
        return unlocked;
    }
}
