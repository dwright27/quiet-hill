﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public abstract class BaseTrigger : MonoBehaviour {

    public AudioClip activationSound;
    public AudioClip postActivationSound;
    public AudioSource source;
    public float activationProbability = 0.6f;
    public ScaryEventType type;
   // public bool canActivate  =false;
   
    private ScaryEventManager manager;

    public void Start()
    {
        var temp = GameObject.FindGameObjectWithTag("ScaryEventManager");
        manager = (temp as GameObject).GetComponent<ScaryEventManager>();
    }

    public virtual void OnTriggerEnter(Collider collider)
    {
        if (!manager.EventActivated(type)) return;

        float seed = Random.Range(0.0f, 11.0f);
        bool canActivate = activationProbability >= 1 / seed;

        if (!canActivate) return;
        Event();
    }

    abstract public void Event();

    protected void EventEnded()
    {
        manager.EventEnded(type);
    }
}
