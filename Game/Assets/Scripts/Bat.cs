﻿using UnityEngine;
using System.Collections;

public class Bat : MonoBehaviour
{
	private float time;
	public float lifeTime = 5.0f;
	public float force = 10.0f;
	// Use this for initialization
	void Start ()
	{
		time = lifeTime;
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.LookAt( transform.position + transform.forward );
		rigidbody.velocity = ( force * transform.forward );
		time -= Time.deltaTime;
		if ( time <= 0 ) Destroy( gameObject );
	}
}