﻿using UnityEngine;
using System.Collections;

public class BegeiningCutscene : MonoBehaviour {

    public GameObject player;
    public GameObject ghost;
    public GameObject door;
    public GameObject targetObject;

    private FirstPersonDrifter playerScript;

    private float timerCounter = .0f;
    private float moveFarwordTimeDuration = 1.5f;
    private float currentXCameraRot = .0f;
    private float turnCameraTimer = .0f;

    private bool isTriggered = false;
    private bool moveForwad = false;
    private bool canOpenDoor = false;
    private bool frontDoorOpened = false;
    private bool canZoomIn = false;
    private bool canTurnBack = false;
    private bool eventEnded = false;
    private bool enablePlayerControl = false;

    private delegate void Callback();

	// Use this for initialization
	void Start () {
        playerScript = player.GetComponent<FirstPersonDrifter>();
	}

    void FrontDoorOpenedCallback()
    {
        moveForwad = false;
        frontDoorOpened = true;
        canOpenDoor = true;
    }

    void FrontDoorClosedCallback()
    {
        moveForwad = false;
        canZoomIn = true;
    }

    void zoomInGhostCallback()
    {
        canZoomIn = false;
        canTurnBack = true;
        Camera.main.GetComponent<CameraZoom>().DisableZoomIn();
    }



	// Update is called once per frame
	void Update () {

       
            if (moveForwad && !frontDoorOpened)
            {
                MoveForward(.0f, moveFarwordTimeDuration, FrontDoorOpenedCallback);
            }

            if (canOpenDoor)
            {
                var doorScript = door.GetComponentInChildren<Door>();
                doorScript.Open();
                canOpenDoor = false;
                moveForwad = true;
            }

            if (moveForwad && frontDoorOpened)
            {
                moveFarwordTimeDuration = 1.5f;
                MoveForward(.5f, moveFarwordTimeDuration, FrontDoorClosedCallback);
            }

            if (canZoomIn)
            {
                ghost.GetComponent<GirlCutSceneScript>().StartAnimating = true;

                ZoomInToGhost(1.0f, 3.0f, zoomInGhostCallback);
            }

            if (canTurnBack )
            {
                if (player.GetComponent<MouseLook>().TurnHorizontal(currentXCameraRot, 180.0f))
                {
                    currentXCameraRot += .05f;
                }
                else
                {
                    var doorScript = door.GetComponentInChildren<Door>();
                    doorScript.Close();      
                    eventEnded = true;
                    timerCounter += .0f;
                    enablePlayerControl = true;
                }
            }

            if (enablePlayerControl && turnCameraTimer > 2.0f)
            {
                playerScript.EnablePlayerControl();
                Destroy(this);
            }
            else if (enablePlayerControl)
            {
                turnCameraTimer += Time.deltaTime;
            }
	}

    void StartCutScene()
    {
        if (!isTriggered)
        {
            InitialSetup();
        }
    }

    void InitialSetup()
    {
        playerScript.DisablePlayerControl();
        playerScript.gameObject.transform.rotation = Quaternion.LookRotation(new Vector3(.0f, .0f, 1.0f));
        playerScript.gameObject.transform.rotation = Quaternion.AngleAxis(.0f, new Vector3(1.0f, .0f,.0f));
        playerScript.gameObject.transform.position = new Vector3(-10, 15, -188);
        moveForwad = true;
        moveFarwordTimeDuration = 1.5f;
        isTriggered = true;
    }

    void MoveForward(float delay, float timeDuration, Callback callback)
    {
        timerCounter += Time.deltaTime;
        if (timerCounter > delay && timerCounter <= timeDuration + delay)
        {
            playerScript.MoveForward((targetObject.transform.position - player.gameObject.transform.position).normalized);
        }
        else if(timerCounter >= timeDuration + delay)
        {
            timerCounter = .0f;
            callback();
        }
    }

    void ZoomInToGhost(float delay, float timeDuration, Callback callback)
    {
        timerCounter += Time.deltaTime;
        if (timerCounter > delay && timerCounter <= timeDuration + delay)
        {
            Camera.main.GetComponent<CameraZoom>().ZoomIn();
        }
        else if (timerCounter >= timeDuration + delay)
        {
            timerCounter = .0f;
            callback();
        }
    }

    public void OnTriggerEnter(Collider collider)
    {
        StartCutScene();
    }
}
