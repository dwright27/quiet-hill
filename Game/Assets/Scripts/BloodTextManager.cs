﻿using UnityEngine;
using System.Collections;

public class BloodTextManager : MonoBehaviour {
    public GameObject bloodTextCollection;
    private float timer = .0f;
    private float fadeTime = 12.0f;
    private bool enableFadeIn = false;
    private bool enableFadeOut = false;

	// Use this for initialization
	void Start () {
        enableFadeOut = true;
	}

    public void OnTriggerEnter(Collider c)
    {
        EnableFadeIn();
    }

    public void EnableFadeIn()
    {
        enableFadeIn = true;
        enableFadeOut = false;
    }

    public void EnableFadeOut()
    {
        enableFadeOut = true;
        enableFadeIn = false;
    }

    private void FadeIn()
    {
        if (timer < fadeTime && enableFadeIn)
        {
            var texts = bloodTextCollection.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < texts.Length; i++)
            {
                var originalColor = texts[i].renderer.material.color;
                originalColor.a += 0.2f * Time.deltaTime;

                if (originalColor.a > 1.0f) originalColor.a = 1.0f;
                    
                texts[i].renderer.material.color = originalColor;
            }
            timer += Time.deltaTime;

        }
        else
        {
            timer = .0f;
            enableFadeIn = false;
        }
        
    }

    private void FadeOut()
    {
        if (timer < fadeTime && enableFadeOut)
        {
            var texts = bloodTextCollection.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < texts.Length; i++)
            {
                var originalColor = texts[i].renderer.material.color;
                if(originalColor.a > 0.0f)
                    originalColor.a -= 0.1f;
                texts[i].renderer.material.color = originalColor;
            }

            timer += Time.deltaTime;
        }
        else
        {
            timer = .0f;
            enableFadeOut = false;

        }
        
    }
	
	// Update is called once per frame
	void Update () {
        FadeIn();
        FadeOut();
	}
}
