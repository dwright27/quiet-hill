﻿using UnityEngine;
using System.Collections;

public class CollisionSound : MonoBehaviour
{
		public float minImpactForce = 2;
		public AudioClip [] collideSounds;
        private int currIndex = 0;
        
		// Use this for initialization
		void Start ()
		{
				gameObject.audio.clip = collideSounds[0];
		}

		void OnCollisionEnter (Collision collide)
		{
            if (audio.clip.isReadyToPlay && !audio.isPlaying)
            {
                gameObject.audio.Play();
                gameObject.audio.clip = collideSounds[currIndex];
                currIndex++;
                if (currIndex > collideSounds.Length)
                    currIndex = 0;
            }

			//if (collide.relativeVelocity.magnitude > minImpactForce) {
			//		gameObject.audio.Play ();
			//}
		}
}