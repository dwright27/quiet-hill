﻿using UnityEngine;
using System.Collections;

public class ContactedPlayer : MonoBehaviour {

    public bool Contacted = false;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            Contacted = true;
        }
    }
}
