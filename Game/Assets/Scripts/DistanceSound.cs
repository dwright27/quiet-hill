﻿using UnityEngine;
using System.Collections;

public class DistanceSound : MonoBehaviour {
    
    public GameObject reference;
    public AudioSource source;
    public float minDistance = 7;
    private float currDistance = 0;
    public bool enabled = true;

	void Start () {
        source.volume = 0;
        if (!reference)
        {
            Debug.LogError("Set the reference for the direction sound");
        }
        source.loop = true;
        source.playOnAwake = true;
	}
	
	void Update () {
        if (enabled)
        {
            float test = GetCurrentDistanceAway();
            Debug.Log("CurrDistance: " + test);

            if (currDistance < minDistance)
            {
                test = (minDistance - currDistance) / minDistance;
                Debug.Log("Girl Drums Audio level: " + test);
                source.volume = test;
            }
        }
	}

    private float GetCurrentDistanceAway()
    {
        float x, z;
        x = transform.position.x - reference.transform.position.x;
        x = x * x;
        z = transform.position.z - reference.transform.position.z;
        z = z * z;
        float mag = (transform.position - reference.transform.position).magnitude;
        currDistance = mag;
        //currDistance = Mathf.Sqrt(x + z);
        return currDistance;
    }

    public void Toggle()
    {
        enabled = !enabled;
    }
}
