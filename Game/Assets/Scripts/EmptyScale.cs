﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EmptyScale : Investigatable {

    public GameObject scaleBody;
    public GameObject[] bottles;


    // Use this for initialization
	void Start () {
        foreach (var b in bottles)
        {
            b.renderer.enabled = false;
        }
	}

    void MoveDown()
    {

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void Investigate(GameObject player = null)
    {
        var playerObj = player.GetComponent<FirstPersonDrifter>();

        if (playerObj.holdingItem != null)
        {
            var scale = scaleBody.GetComponent<ScalePuzzle>();
            scale.BottleAdded();
            bottles[scale.BottleCount - 1].renderer.enabled = true;
            bottles[scale.BottleCount - 1].rigidbody.useGravity = true;
            Destroy(playerObj.holdingItem);
        }
    }

    public override InvestigatableEnum getInvestigateState(GameObject player = null)
    {
        return InvestigatableEnum.Place_Bottle;
    }

    public override bool canInvestigate(GameObject player = null)
    {
        return scaleBody.GetComponent<ScalePuzzle>().BottleCount < 4;
    }
}
