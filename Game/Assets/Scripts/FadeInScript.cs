﻿using UnityEngine;
using System.Collections;

public class FadeInScript : MonoBehaviour {


    public float startingAlpha = 0.0f;
    public float endingAlpha = 1.0f;
    public float fadeSpeed = 2.0f;


    private float timer = 0.0f;
	private bool _fade;
	public bool Fade
    {
        get
		{
			return _fade;
		}
        set
		{
			if(value)
			{
				Done = false;
				
			}
			_fade = value;
		}
    }
	public bool Done{get; private set;}

	// Use this for initialization
	void Start () {
        Fade = false;
		Done = false;
	}
	
	// Update is called once per frame
	void Update () {
	    if(Fade)
        {
            Color tempColor = guiTexture.color;
            timer += Time.deltaTime;
            tempColor.a = Mathf.Lerp(startingAlpha, endingAlpha, timer * fadeSpeed);
            guiTexture.color = tempColor;
            if (tempColor.a == endingAlpha)
            {
                timer = 0.0f;
                Fade = false;
				Done = true;
            }
        }
	}
}
