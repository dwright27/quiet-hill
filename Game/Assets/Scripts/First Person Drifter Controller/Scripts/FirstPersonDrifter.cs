﻿// original by Eric Haines (Eric5h5)
// adapted by @torahhorse
// http://wiki.unity3d.com/index.php/FPSWalkerEnhanced

using UnityEngine;
using System.Collections;
using System;

[RequireComponent (typeof (CharacterController))]
public class FirstPersonDrifter: MonoBehaviour
{
    public float walkSpeed = 6.0f;
    public float runSpeed = 10.0f;

    public AudioClip footStep;
    public AudioClip runningFootStep;
    public AudioClip scream;
    public AudioClip floorCreak;
    public AudioClip heatbeat;


    public GameObject holdingItem;
    public GUIText ActionHint;
    public GUITexture Death;
    public GUITexture EyeBallTexture;
    public InventoryContainer inventory = new InventoryContainer();

    private bool hidden;

    public bool IsHidden
    {
        get;
        set;
    }

    public bool IsAlive { get; set; }

    public bool PlayerControl
    {
        get { return playerControl; }
    }

	private float oldDrag;
	private int oldLayer;
    public void HoldItem(GameObject item)
    {

		if ( holdingItem != null )
		{
			holdingItem.GetComponent<InvestigateHold>().holding = false;
			holdingItem.rigidbody.useGravity = true;
			holdingItem.rigidbody.drag = oldDrag;
			holdingItem.layer = oldLayer;
			holdingItem = null;

		}
        //item.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 12.0f;
        //item.transform.rotation = Camera.main.transform.rotation;
        //item.transform.Translate(0.0f, -4.5f, 2.0f);
        //item.transform.localRotation = Quaternion.EulerAngles(180, -180, 180.0f);
        //item.transform.rotation = Quaternion.EulerAngles(180, -180, 180.0f);
        //item.transform.parent = Camera.main.transform;
		item.rigidbody.useGravity = false;
		oldDrag = item.rigidbody.drag;
		item.rigidbody.drag = 10;
		oldLayer = item.layer;
		item.layer = LayerMask.NameToLayer( "Ignore Raycast" );
        Camera.main.GetComponent<CameraZoom>().DisableRightClickZoomIn();
        holdingItem = item;
    }

    private bool showHidingText = false;
    private float hidingTextDuration = 5.0f;
    private float hidingTextTimeCounter = .0f;

    // If true, diagonal speed (when strafing + moving forward or back) can't exceed normal move speed; otherwise it's about 1.4 times faster
    private bool limitDiagonalSpeed = true;
 
    public bool enableRunning = true;
 
    public float jumpSpeed = 40.0f;
    public float gravity = 10.0f;
 
    // Units that player can fall before a falling damage function is run. To disable, type "infinity" in the inspector
    private float fallingDamageThreshold = 10.0f;
 
    // If the player ends up on a slope which is at least the Slope Limit as set on the character controller, then he will slide down
    public bool slideWhenOverSlopeLimit = false;
 
    // If checked and the player is on an object tagged "Slide", he will slide down it regardless of the slope limit
    public bool slideOnTaggedObjects = false;
 
    public float slideSpeed = 5.0f;
 
    // If checked, then the player can change direction while in the air
    public bool airControl = true;
 
    // Small amounts of this results in bumping when walking down slopes, but large amounts results in falling too fast
    public float antiBumpFactor = .75f;
 
    // Player must be grounded for at least this many physics frames before being able to jump again; set to 0 to allow bunny hopping
    public int antiBunnyHopFactor = 1;

    //In game objects
    public GameObject flashLight;

    public AudioSource heartBeatSource;

    private Vector3 moveDirection = Vector3.zero;
    private bool grounded = false;
    private CharacterController controller;
    private Transform myTransform;
    private float speed;
    private RaycastHit hit;
    private float fallStartLevel;
    private bool falling;
    private float slideLimit;
    private float rayDistance;
    private Vector3 contactPoint;
    private bool playerControl = false;
    private int jumpTimer;

    private float footStepTimer;
    private bool wasRunning = false;

    public void EnablePlayerControl()
    {
        playerControl = true;
        EyeBallTexture.enabled = true;
    }

    public void DisablePlayerControl()
    {
        playerControl = false;
        EyeBallTexture.enabled = false;

    }

    void Start()
    {
        controller = GetComponent<CharacterController>();
        myTransform = transform;
        speed = walkSpeed;
        rayDistance = controller.height * .5f + controller.radius;
        slideLimit = controller.slopeLimit - .1f;
        jumpTimer = antiBunnyHopFactor;
        footStepTimer = 0;
        hidden = false;
        IsAlive = true;
        playerControl = true;
        gameObject.audio.clip = footStep;
        IsHidden = false;
		
    }

    public void Die()
    {
        Death.guiTexture.pixelInset = new Rect(-Screen.width  / 2, -Screen.height / 2, Screen.width, Screen.height);
       
        if (IsAlive)
        {
            gameObject.audio.clip = (scream);
        }

        if (!gameObject.audio.isPlaying)
        {
            gameObject.audio.Play();
        }
        IsAlive = false;

    }

    void playHeartbeat()
    {
        heartBeatSource.PlayOneShot(heatbeat);
        //yield return new WaitForSeconds(2);
    }
 
    void Update() {

        if (IsAlive && playerControl)
        {
            float inputX = Input.GetAxis("Horizontal");
            float inputY = Input.GetAxis("Vertical");
            // If both horizontal and vertical are used simultaneously, limit speed (if allowed), so the total doesn't exceed normal move speed
            float inputModifyFactor = (inputX != 0.0f && inputY != 0.0f && limitDiagonalSpeed) ? .7071f : 1.0f;

            if (grounded)
            {
                bool sliding = false;
                // See if surface immediately below should be slid down. We use this normally rather than a ControllerColliderHit point,
                // because that interferes with step climbing amongst other annoyances
                if (Physics.Raycast(myTransform.position, -Vector3.up, out hit, rayDistance))
                {
                    if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
                        sliding = true;
                }
                // However, just raycasting straight down from the center can fail when on steep slopes
                // So if the above raycast didn't catch anything, raycast down from the stored ControllerColliderHit point instead
                else
                {
                    Physics.Raycast(contactPoint + Vector3.up, -Vector3.up, out hit);
                    if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
                        sliding = true;
                }

                // If we were falling, and we fell a vertical distance greater than the threshold, run a falling damage routine
                if (falling)
                {
                    falling = false;
                    if (myTransform.position.y < fallStartLevel - fallingDamageThreshold)
                        FallingDamageAlert(fallStartLevel - myTransform.position.y);
                }

                if (enableRunning)
                {
                    speed = Input.GetButton("Run") ? runSpeed : walkSpeed;
                    wasRunning = Input.GetButton("Run") || wasRunning;
                    gameObject.audio.clip = Input.GetButton("Run") ? runningFootStep : footStep;
                }


                if( wasRunning && Input.GetButtonUp("Run"))
                {
                    //StartCoroutine(playHeartbeat());
                    heartBeatSource.Play();
                    wasRunning = false;
                }

                if (Input.GetButton("Fire2") && holdingItem == null)
                {
                    Camera.main.GetComponent<CameraZoom>().EnableRightClickZoomIn();
                }

                //// If sliding (and it's allowed), or if we're on an object tagged "Slide", get a vector pointing down the slope we're on
                //if ( (sliding && slideWhenOverSlopeLimit) || (slideOnTaggedObjects && hit.collider.tag == "Slide") ) {
                //    Vector3 hitNormal = hit.normal;
                //    moveDirection = new Vector3(hitNormal.x, -hitNormal.y, hitNormal.z);
                //    Vector3.OrthoNormalize (ref hitNormal, ref moveDirection);
                //    moveDirection *= slideSpeed;
                //    playerControl = false;
                //}

                // Otherwise recalculate moveDirection directly from axes, adding a bit of -y to avoid bumping down inclines
                //else {
                moveDirection = new Vector3(inputX * inputModifyFactor, -antiBumpFactor, inputY * inputModifyFactor);

                //play footstep sound after moving a certain amount
                //change current audio to footstep sound randomly for floor board creaking
                footStepTimer += Time.deltaTime;
                bool footstepSoundSwitched = false;
                if (footStepTimer > 10)
                {
                    gameObject.audio.clip = floorCreak;
                    footstepSoundSwitched = true;
                    footStepTimer = 0;
                    //gameObject.audio.Play();
                }

                if (!gameObject.audio.isPlaying && (Math.Abs(moveDirection.x) > 0.1f || Math.Abs(moveDirection.z) > 0.1f))
                {
                    //creaking floor boards 
                    gameObject.audio.Play();
                }
                else if (gameObject.audio.clip != floorCreak && !gameObject.audio.isPlaying)
                {
                    gameObject.audio.Stop();
                }
                if (footstepSoundSwitched)
                {
                    gameObject.audio.clip = footStep;
                }
               
                moveDirection = myTransform.TransformDirection(moveDirection) * speed;
                playerControl = true;

                //}

                // Jump! But only if the jump button has been released and player has been grounded for a given number of frames
                if (!Input.GetButton("Jump"))
                    jumpTimer++;
                else if (jumpTimer >= antiBunnyHopFactor)
                {
                    moveDirection.y = jumpSpeed;
                    jumpTimer = 0;
                }

                if(holdingItem != null && Input.GetMouseButtonUp(1))
                {
                    holdingItem.GetComponent<InvestigateHold>().holding = false;
					holdingItem.rigidbody.useGravity = true;
					holdingItem.rigidbody.drag = oldDrag;
					holdingItem.layer = oldLayer;
                    holdingItem = null;

                }
				else if(holdingItem != null)
				{
					Vector3 targetPos = Camera.main.transform.position + Vector3.Normalize(Camera.main.transform.forward ) * 10.0f;
					if ( ( holdingItem.transform.position - transform.position ).magnitude > (targetPos - transform.position).magnitude )
					{
						holdingItem.transform.position = targetPos;
					}
					else
					{
						holdingItem.rigidbody.AddForce( ( targetPos - holdingItem.transform.position ) * 100 );
					}
					holdingItem.transform.rotation = Camera.main.transform.rotation;
				}

                //check if the cursor hits any objects
                RaycastHit hitInfo;
                var ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.0f));
                Investigatable item;
				if ( Physics.Raycast( ray, out hitInfo, 20 ) )
                {
                    item = hitInfo.collider.GetComponent<Investigatable>();
                    //bool isObject = (item is InvestigateHold);

					bool investigate = !( (item is InvestigateHold) && holdingItem );
					//if(!isObject)
					//{
					//	investigate = Physics.Raycast( ray, out hitInfo, 10 );
					//}

                    if (investigate)
                    {

                        if (item != null && item.canInvestigate(gameObject))
                        {
                            ActionHint.guiText.pixelOffset.Set(0.0f, -20.0f);
                            ActionHint.guiText.text = item.getInvestigateState(gameObject).ToString().Replace('_', ' ');


                            if (Input.GetMouseButtonDown(0))
                            {
                                item.Investigate(gameObject);
                            }
                        }
                        else
                        {
                            ActionHint.guiText.pixelOffset.Set(0.0f, -20.0f);
                            ActionHint.guiText.text = "";
                        }
                        var sheet = hitInfo.collider.GetComponentInChildren<BedSheet>();
                        if (sheet != null)
                        {
                            sheet.GetComponentInParent<InteractiveCloth>()
                                .AddForceAtPosition(new Vector3(.0f, 3.0f, .0f),
                                sheet.transform.position, 1.0f);

                            ActionHint.guiText.text = sheet.getInvestigateState(sheet.gameObject).ToString().Replace('_', ' ');
                        }
                    }
                }
                else
                {
                    ActionHint.guiText.pixelOffset.Set(0.0f, -20.0f);
                    ActionHint.guiText.text = "";
                }

            }
            else
            {
                // If we stepped over a cliff or something, set the height at which we started falling
                if (!falling)
                {
                    falling = true;
                    fallStartLevel = myTransform.position.y;
                }

                // If air control is allowed, check movement but don't touch the y component
                if (airControl && playerControl)
                {
                    moveDirection.x = inputX * speed * inputModifyFactor;
                    moveDirection.z = inputY * speed * inputModifyFactor;
                    moveDirection = myTransform.TransformDirection(moveDirection);
                }

            }



            // Apply gravity
            moveDirection.y -= gravity * Time.deltaTime;

            // Move the controller, and set grounded true or false depending on whether we're standing on something
            grounded = (controller.Move(moveDirection * Time.deltaTime) & CollisionFlags.Below) != 0;

        }

        //flashLight.GetComponent<FlashLightScript>().isFlickering = true;

        if (showHidingText && hidingTextTimeCounter < hidingTextDuration)
        {
            ActionHint.guiText.text = "Hide, Now";
            hidingTextTimeCounter += Time.deltaTime;
        }
        else if(ActionHint.guiText.text.Equals("Hide, Now"))
        {
            ActionHint.guiText.text = "";
            hidingTextTimeCounter = .0f;
            showHidingText = false;
        }

        
    }

    public void ShowHidingText()
    {
         showHidingText = true;
    }
  
    // Store point that we're in contact with for use in FixedUpdate if needed
    void OnControllerColliderHit (ControllerColliderHit hit) {
        contactPoint = hit.point;
    }
 
    // If falling damage occured, this is the place to do something about it. You can make the player
    // have hitpoints and remove some of them based on the distance fallen, add sound effects, etc.
    void FallingDamageAlert (float fallDistance)
    {
        //print ("Ouch! Fell " + fallDistance + " units!");   
    }

    public void MoveForward(Vector3 dir)
    {
        moveDirection = myTransform.TransformDirection(dir) * speed;
        if (!gameObject.audio.isPlaying && (Math.Abs(moveDirection.x) > 0.1f || Math.Abs(moveDirection.z) > 0.1f))
        {
            //creaking floor boards 
            gameObject.audio.Play();
        }

        controller.Move(moveDirection * Time.deltaTime);
    }
    
}