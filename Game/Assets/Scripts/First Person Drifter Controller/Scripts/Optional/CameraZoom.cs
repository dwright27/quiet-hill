﻿// by @torahhorse

using UnityEngine;
using System.Collections;

// allows player to zoom in the FOV when holding a button down
[RequireComponent (typeof (Camera))]
public class CameraZoom : MonoBehaviour
{
	public float zoomFOV = 30.0f;
	public float zoomSpeed = 9f;
	
	private float targetFOV;
	private float baseFOV;
    private bool scriptingZoomIn = false;
    private bool canZoomIn = true;

	void Start ()
	{
		SetBaseFOV(camera.fieldOfView);
	}

    public void ZoomIn()
    {
        scriptingZoomIn = true;
    }

    public void DisableZoomIn()
    {
        scriptingZoomIn = false;
    }

    public void DisableRightClickZoomIn()
    {
        canZoomIn = false;
    }

    public void EnableRightClickZoomIn()
    {
        canZoomIn = true;
    }

	void Update ()
	{
        bool isRunning = Input.GetButton("Run") && Input.GetButton("Vertical");
        if ((Input.GetButton("Fire2") || isRunning || scriptingZoomIn) && canZoomIn)
		{
			targetFOV = isRunning ? zoomFOV * 2.0f : zoomFOV;
		}
		else
		{
			targetFOV = baseFOV;
		}
		
		UpdateZoom();
	}
	
	private void UpdateZoom()
	{
		camera.fieldOfView = Mathf.Lerp(camera.fieldOfView, targetFOV, zoomSpeed * Time.deltaTime);
	}
	
	public void SetBaseFOV(float fov)
	{
		baseFOV = fov;
	}
}
