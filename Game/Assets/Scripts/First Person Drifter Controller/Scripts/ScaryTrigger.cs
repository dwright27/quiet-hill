﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;

//Attach to a gameobject with a box collider used as a trigger
    //place this object where you want an event trigger
    //you can set a delay time before the signal is sent to the roommanager
    //Signals to the Manager when the player enters the trigger
    //Must have a reference to the roomManager they belong to

//scary event manager not working so I disabled the inheritance from basetrigger
public class ScaryTrigger : MonoBehaviour{

    private delegate void Del();
    private Del activateTrig;
    private float timer = 0;
    private bool timerActive = false;

    public GameObject manager;
    public float delayTime;

	void Start () 
    {
        //base.Start();
        if (manager == null)
        {
            Debug.Log("No manager attached to Scary Trigger");
        }
        else
        {
            //activateTrig = manager.GetComponent<RoomManager>().activateTrigger;
            activateTrig = test;
        }
	}

    void test()
    {
        manager.GetComponent<RoomManager>().activateTrigger();
    }

    //sends signal to the room manager after the delay
	void Update () 
    {
        if (timerActive)
        {
            timer += Time.deltaTime;
            if (timer >= delayTime)
            {
                activateTrig(); 
                timer = 0;
                timerActive = false;
            }
        }
	}

    //starts the timer upont collision with player
    public void OnTriggerEnter(Collider _collide)
    {
        //base.OnTriggerEnter(_collide);
        if (_collide.tag == "Player")
        {
            Debug.Log("scary trigger collide with player");
            timerActive = true;
        }
    }
}