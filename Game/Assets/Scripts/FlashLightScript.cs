﻿using UnityEngine;
using System.Collections;

public class FlashLightScript : MonoBehaviour {

    private Light lightBulb;
    public float delayAmount = 0.1f;

    private float delayLeft = 0.0f;
    private bool _flicker = false;
    private bool _flashLightOn = true;

    public bool On
    {
         get{return _flashLightOn;}
         set{
            _flashLightOn = value;
            lightBulb.enabled = value;
        }
    }

    public bool isFlickering
    {
         get { return _flicker; }
         set { _flicker = value; }
    }


	// Use this for initialization
	void Start () {

        lightBulb = transform.GetComponentInChildren<Light>();// bulb.GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        if (On)
        {
            if (isFlickering)
            {
                if (delayLeft <= 0.0f)
                {
                    lightBulb.enabled = !lightBulb.enabled;
                    delayLeft = delayAmount;
                }
                delayLeft -= Time.deltaTime;
            }
        }
	}

}
