﻿using UnityEngine;
using System.Collections;


    public class Collectable : Investigatable
    {
		public bool oneTimeUse = false;
		protected bool used = false;
		public virtual bool Use( GameObject obj = null ) { return false; }

		public override void Investigate( GameObject player = null )
		{
			var p = player.GetComponent<FirstPersonDrifter>();
            p.inventory.AddItem(this);

            var keys = p.inventory.GetItems<Key>();
            
            foreach(var k in keys)
            {
                var key = k as Key;
                Debug.Log(key.id);
             }
			for ( int i = 0; i < gameObject.transform.childCount;++i )
			{
				gameObject.transform.GetChild( i ).gameObject.SetActive( false );
			}
			if ( collider ) collider.enabled = false;
			else if(GetComponentInChildren<Collider>())
			{
				GetComponentInChildren<Collider>().enabled = false;
			}
			if ( renderer ) renderer.enabled = false;
			else if(GetComponentInChildren<Renderer>())
			{
				GetComponentInChildren<Renderer>().enabled = false;
			}
			if ( audio )
			{
				audio.enabled = true;
				audio.Play();
			}
			else if ( GetComponentInChildren<AudioSource>() )
			{
				GetComponentInChildren<AudioSource>().enabled = true;
				audio.Play();
			}
			if(GetComponent<Glow>())
			{
				GetComponent<Glow>().enabled = false;
				GetComponent<Glow>().HaloEnabled = false;
			}
			else if(GetComponentInChildren<Glow>())
			{
				GetComponentInChildren<Glow>().enabled = false;
				GetComponentInChildren<Glow>().HaloEnabled = false;
			}
			if ( GetComponent<Light>() )
			{
				GetComponent<Light>().enabled = false;
			}
			else if ( GetComponentInChildren<Light>() )
			{
				GetComponentInChildren<Light>().enabled = false;
			}
		}

		public override InvestigatableEnum getInvestigateState(GameObject player = null)
		{
			return InvestigatableEnum.Pick_Up;
		}

		public override bool canInvestigate( GameObject player = null )
		{
			return true;
		}
		public override void Update()
		{
		}
	}