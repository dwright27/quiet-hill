﻿using System.Collections;
using System.Collections.Generic;
    
public class InventoryContainer {

    private IList<Collectable> objects = new List<Collectable>();

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    public bool AddItem(Collectable item)
    {   
        if(!objects.Contains(item))
            objects.Add(item);
        return true;
    }

    public List<Collectable> GetItems<T>()
    {
        List<Collectable> items = new List<Collectable>();

        foreach(var i in objects)
        {
            if (i is T)
            { 
                items.Add(i);
            }
        }

        return items;
    }

    
}
