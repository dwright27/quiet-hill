﻿using UnityEngine;
using System.Collections;

public class GameManagerScript : MonoBehaviour {

    public GameObject ghostPrefab;
    public GameObject gameOver;
    public GameObject gameOverWin;
    public GameObject combinationManager;
    public GameObject ghostTrigger;
    public  GameObject KitchenTriggerPrefab;
    public  GameObject HallTriggerPrefab;
    public ContactedPlayer bedroomContact;
    public CombinationPuzzleManager kitchenPuzzle;
    public Key finalKey;
    public float minumumGhostSpawnDistance = 50;

    private GameObject kitchenTrigger = null;
    private GameObject hallTrigger = null;

    public float ghostRespawnWait = 30;
    public float ghostAliveTime = 30;
    public float distanceBeforeFlicker = 200;
    public float distanceDeath = 10;
    public AudioClip secondarySong;

    private float ghostAliveTimeLeft;
    private float ghostRespawnWaitTimeLeft;
    private bool ghostSummoned = false;

    public AudioClip ghostFar;
    public GameObject player;
    public GameObject[] bloodTexts = new GameObject[2];
    public BedroomCutScene bedroomGirl;
    private FlashLightScript playerFlashLight;
    private GameObject ghost;
    private GhostScript ghostScript;
    private FadeInScript gameOverScript;
    private FadeInScript gameOverWinScript;
    private SummonGhostScript ghostSummoner;
    private CombinationPuzzleManager puzzleManager;
    private bool playerWon;
    private bool _ghostAllowed;
    private bool playerLost = false;
    public delegate void GhostSummonedDelegate();
    public static GhostSummonedDelegate InitialSummon;
    private bool inKitchen = false;
	public GameObject staticScreen;
	private GUITexture[] _staticScreen;
	public AudioSource staticScreenSound;
    public bool GhostAllowed
    {
        get { return _ghostAllowed; }
        private set { _ghostAllowed = value; if(!_ghostAllowed)RemoveGhost(); }
    }



    private Vector3 [] ghostSpawnPoints = 
    {
        new Vector3(-70.0f, 2.0f, -40.0f),
        new Vector3(-43.0f, 2.0f, 86.0f),
        new Vector3(-34.0f, 2.0f, -51.0f),
        new Vector3(-12.0f, 2.0f, -49.0f)
    };


	// Use this for initialization
	void Start () 
    {
       // player = (GameObject)Instantiate(playerPrefab, playerPrefab.transform.position, playerPrefab.transform.rotation);
        ghostRespawnWaitTimeLeft = ghostRespawnWait;
        playerFlashLight = player.transform.FindChild("Main Camera").FindChild("FlashLight").GetComponent<FlashLightScript>();
        audio.Play();
        gameOverScript = gameOver.GetComponent<FadeInScript>();
        gameOverWinScript = gameOverWin.GetComponent<FadeInScript>();

        GhostAllowed = false;
        ghostSummoner = ghostTrigger.GetComponent<SummonGhostScript>();
        puzzleManager = combinationManager.GetComponent<CombinationPuzzleManager>();
        
        for (int i = 0; i < bloodTexts.Length; i++)
		{
            bloodTexts[i].GetComponent<BloodTextManager>().EnableFadeOut();
            InitialSummon += bloodTexts[i].GetComponent<BloodTextManager>().EnableFadeIn;	 
		}

        InitialSummon += player.GetComponent<FirstPersonDrifter>().ShowHidingText;
		if ( staticScreen ) _staticScreen = staticScreen.GetComponentsInChildren<GUITexture>();
    }
	
	// Update is called once per frame
	void Update () 
    {
		if(gameOverScript.Done)
		{
			Application.LoadLevel( 2 );
		}

        if(ghostSummoner != null && ghostSummoner.ContactedPlayer)  
        {
            ghostSummoner = null;
            Destroy(ghostTrigger);
            ghostTrigger = null;
            GhostAllowed = true;
        }

        UpdateTriggers();

        if (ghostSummoner == null)
        {
            if (player.GetComponent<FirstPersonDrifter>().IsHidden)
            {
                RemoveGhost();
            }
            else if(!inKitchen)
            {
                GhostAllowed = true;
            }

        }



        if (bedroomContact.Contacted && !playerLost && !playerWon)
        {

            bool found = false;
            foreach(Collectable c in player.GetComponent<FirstPersonDrifter>().inventory.GetItems<Collectable>())
            {
                if(c.GetInstanceID() == finalKey.GetInstanceID())
                {
                    found = true;
                }
            }

            RemoveGhost();
            if (found)
            {
                //player.GetComponent<FirstPersonDrifter>().Die();
                gameOverWinScript.Fade = true;
                playerWon = true;
            }
            else
            {
                RemoveGhost();

                gameOverScript.Fade = true;
                playerLost = true;
            }
            Destroy(bedroomGirl);
            GhostAllowed = false;

        }


        if (GhostAllowed)
        {
            GhostSpawnUpdate();
            FlashlightUpdate();
        }

    //    FinalPuzzleUpdate();

        PopGhostInfrontOfPlayer();

		foreach(GUITexture i in _staticScreen)
		{
			i.enabled = false;
		}
		staticIndex = ++staticIndex % _staticScreen.Length;
		if(ghost)
		{
			float amount = (ghost.transform.position - player.transform.position).magnitude / 30.0f;
			_staticScreen[staticIndex].enabled = true;
			_staticScreen[staticIndex].pixelInset = new Rect( -Screen.width / 2, -Screen.height / 2, Screen.width, Screen.height );
			_staticScreen[staticIndex].color = new Color( 0.5f, 0.5f, 0.5f, 0.5f - (amount * 0.5f) );
			if(staticScreenSound)
			{
				staticScreenSound.volume = 1 - amount;
				if ( !staticScreenSound.isPlaying ) staticScreenSound.Play();
			}
		}
    }

	static int staticIndex = 0;
    void UpdateTriggers()
    {
        if (kitchenPuzzle.isSolved() && kitchenTrigger == null)
        {
            kitchenTrigger = (GameObject)Instantiate(KitchenTriggerPrefab);
            hallTrigger = (GameObject)Instantiate(HallTriggerPrefab);
            GhostAllowed = false;
            bedroomContact.enabled = true;
            bedroomGirl.enabled = true;
            bedroomGirl.Sing = true;
            ghostSummoner = null;
            Destroy(ghostTrigger);
            ghostTrigger = null;
            inKitchen = true;
        }

        if (kitchenTrigger != null)
        {
            if (kitchenTrigger.GetComponent<ContactedPlayer>().Contacted && GhostAllowed)
            {
                GhostAllowed = false;
                bedroomGirl.Sing = true;
                inKitchen = true ;
            }

            if (hallTrigger.GetComponent<ContactedPlayer>().Contacted && !GhostAllowed)
            {
                GhostAllowed = true;
                bedroomGirl.Sing = false;
                inKitchen = false;
            }

            kitchenTrigger.GetComponent<ContactedPlayer>().Contacted = false;
            hallTrigger.GetComponent<ContactedPlayer>().Contacted = false;


        }
    }

    void FinalPuzzleUpdate()
    {
        if(puzzleManager.isSolved() && !playerWon)
        {
            //player.GetComponent<FirstPersonDrifter>().Die();
            RemoveGhost();
            gameOverWinScript.Fade = true;
            GhostAllowed = false;
            playerWon = true;
        }
    }


    void FlashlightUpdate()
    {
        if(ghostSummoned)
        {
            float playerDistance = Vector3.Distance(player.transform.position, ghost.transform.position);
           // print(playerDistance);
            if (distanceDeath > playerDistance)
            {
                player.GetComponent<FirstPersonDrifter>().Die();
               
                gameOverScript.Fade = true;
                GhostAllowed = false;
            }
        }
    }

    bool initialSpawn = true;
    void GhostSpawnUpdate()
    {

        

        if (!ghostSummoned)
        {
            ghostRespawnWaitTimeLeft -= Time.deltaTime;

            if (ghostRespawnWaitTimeLeft <= 0.0f)
            {
                if (initialSpawn)
                {
                    initialSpawn = false;
                    audio.clip = secondarySong;
                    audio.Play();
                    if(InitialSummon != null)
                        InitialSummon();
                }
                SummonGhost();

            }
        }
        else
        {
            ghostAliveTimeLeft -= Time.deltaTime;
            if (ghostAliveTimeLeft <= 0.0f)
            {
                RemoveGhost();
            }

            if ((ghostScript != null && ghostScript.ContactedPlayer) || bedroomGirl.DoneAnimating)
            {
                RemoveGhost();
                Destroy(bedroomGirl);
                gameOverScript.Fade = true;
            }
        }
    }

    void SummonGhost()
    {
        //float min = 60.0f;
        //float max = 70.0f;
      //  Vector3 position = new Vector3(Random.Range(player.transform.position.x + min, player.transform.position.x + max), 2.4f, Random.Range(player.transform.position.z + min, player.transform.position.z + max));


        Vector2 unitPosition;//= Random.insideUnitCircle * 2.0f - new Vector2(-1,-1))  * 50.0f) + new Vector2(50.0f,50.0f);
        unitPosition = (Random.insideUnitCircle * 2.0f - new Vector2(-1, -1)).normalized * minumumGhostSpawnDistance;
        Vector3 position = new Vector3(unitPosition.x , 2.0f, unitPosition.y);
        
        //ghostSpawnPoints[Random.Range(0, ghostSpawnPoints.Length)];
        //float distance = Vector3.Distance(position, player.transform.position);
        //print(distance + " : " + position);

        //while(distance < minumumGhostSpawnDistance)
        //{
        //    position = ghostSpawnPoints[Random.Range(0, ghostSpawnPoints.Length)];
        //    distance = Vector3.Distance(position, player.transform.position);
        //    print(distance + " : " + position);
        //    if (distance > minumumGhostSpawnDistance)
        //    {
        //        break;
        //    }
        //}

        SummonGhostAt(position);
    }

    public bool isGhostOut()
    {
        return ghost != null;
    }

    void SummonGhostAt(Vector3 position)
    {
        ghostAliveTimeLeft = ghostAliveTime;
        ghostRespawnWaitTimeLeft = ghostRespawnWait;

        if (ghost != null)
        {
            RemoveGhost();
        }

        ghost = (GameObject)Instantiate(ghostPrefab, position, ghostPrefab.transform.rotation);
        ghostScript = ghost.GetComponent<GhostScript>();
        ghostSummoned = true;
        ghostScript.Player = player;

		GameObject[] spawners = GameObject.FindGameObjectsWithTag( "GhostAppearsSpawn" );
		foreach ( GameObject i in spawners )
		{
			i.GetComponent<Spawner>().spawn();
		}
    }
    void RemoveGhost()
    {
        ghostRespawnWaitTimeLeft = ghostRespawnWait;
        Destroy(ghost);
        ghost = null;
        ghostScript = null;
        ghostSummoned = false;
        
    }

    private float ghostPopinfrontOfPlayerConter = .0f;
    void PopGhostInfrontOfPlayer()
    {
        if (ghost != null)
        {
            var distance = Vector3.Distance(ghost.transform.position, player.transform.position);

            if (distance > 100.0f && ghostPopinfrontOfPlayerConter > 5.0f)
            {
                RaycastHit hitInfo;
                var ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.0f));
                if (!Physics.Raycast(ray, out hitInfo, 50))
                {
                    var dir = ray.direction;
                    dir.y = .0f;
                    ghost.transform.position = ray.origin + dir * 30;
                    Vector3 position = ghost.transform.position;
                    position.y = 2.0f;
                    ghost.transform.position = position;
                }
                ghostPopinfrontOfPlayerConter = .0f;
            }
            else if (distance > 100.0f)
            {
                ghostPopinfrontOfPlayerConter += Time.deltaTime;
            }
        }
    }
}
