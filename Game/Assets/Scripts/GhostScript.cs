﻿using UnityEngine;
using System.Collections;

public class GhostScript : MonoBehaviour
{

    public GameObject Player;
    public float howlRange = 20.0f;
    private bool _contactedPlayer = false;
    private bool playerWithinRange = false;
    public GameObject ghostBody;
    public GameObject ghostHair;
    public float speed = 4.0f;
    public bool ContactedPlayer
    {
        get { return _contactedPlayer; }
        set { _contactedPlayer = value; }
    }

    float InitialDistance;

    // Use this for initialization
    void Start()
    {
        InitialDistance = Vector3.Distance(Player.transform.position, transform.position);
        // ghostBody = transform.FindChild("Body").gameObject;
    }

    // Update is called once per frame
    void Update() 
    {


        if (ghostBody.renderer.material.HasProperty("_Color"))
        {
           
            Color c = ghostBody.renderer.material.GetColor("_Color");
            Color hairColor = ghostHair.renderer.material.GetColor("_Color");
            float alpha =  ((InitialDistance - Vector3.Distance(Player.transform.position, transform.position)) / InitialDistance)/2.0f;

            hairColor.a = alpha;
            c.a = alpha;
            ghostHair.renderer.material.color = hairColor;
            ghostBody.renderer.material.color = c;
        }


        if (howlRange < Vector3.Distance(transform.position, Player.transform.position) && !playerWithinRange)
        {
            playerWithinRange = true;
            audio.Play();

        }

       // FirstPersonDrifter playerScript = Player.GetComponent<FirstPersonDrifter>();


        Vector3 playerPosition = Player.transform.position;
        playerPosition.y = transform.position.y;
        transform.LookAt(playerPosition);
        Vector3 direction = transform.forward * Time.deltaTime  * speed; // -Vector3.Normalize(transform.position - playerPosition) * speed * Time.deltaTime;
        transform.position += direction;
  

	}


    void OnCollisionStay(Collision collision)
    {
        if (collision.transform.tag == "Player")
        {
            ContactedPlayer = true;
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.tag == "Player")
        {
            ContactedPlayer = true;
        }
    }
}
