﻿using UnityEngine;
using System.Collections;

public class GirlSpeak : MonoBehaviour
{
	private GameObject player;
	private float closeSpeakDelay = 0, foundSpeakDelay = 0;
    public AudioSource source;
	public AudioClip CloseSpeak, FoundSpeak;
	public float NextCloseSpeakDelay, NextFoundSpeakDelay;
	public float Range;
	bool inRange = false;
	// Use this for initialization
	void Start ()
	{
		player = GameObject.FindGameObjectWithTag("Player");
        if (!source)
        {
			source = audio;
        }
	}
	
	// Update is called once per frame
	void Update ()
	{
		if ( !source ) return;
		closeSpeakDelay -= Time.deltaTime;
		foundSpeakDelay -= Time.deltaTime;
        if (!source.isPlaying && !inRange && closeSpeakDelay <= 0 && Vector3.Magnitude(player.transform.position - transform.position) <= Range)
		{
            if (CloseSpeak) source.PlayOneShot(CloseSpeak);
			closeSpeakDelay = NextCloseSpeakDelay;
			inRange = true;
		}
		else if ( Vector3.Magnitude( player.transform.position - transform.position ) > Range ) inRange = false;

        else if (!source.isPlaying && foundSpeakDelay <= 0)
		{
			RaycastHit hitinfo;
			var ray = new Ray( transform.position, transform.forward );
			if(Physics.Raycast(ray, out hitinfo, 100))
			{
				if(hitinfo.collider.gameObject.Equals(player))
				{
                    if (FoundSpeak) source.PlayOneShot(FoundSpeak);
					foundSpeakDelay = NextFoundSpeakDelay;
				}
			}
		}
	}
}
