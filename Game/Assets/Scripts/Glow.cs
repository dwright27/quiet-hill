﻿using UnityEngine;
using System.Collections;

public class Glow : MonoBehaviour
{
    public Color color = new Color(1,1,1,1);
    public float playerDistanceBeforeGlow = 20.0f;
	public Material glowMaterial;
	public float speed = 5;
	public bool glowChildren = true;
    private GameObject player;
	private float currentWave = 0;
    private bool _haloEnabled;
    public bool triggerGlow;
    public bool HaloEnabled
    {
        get { return _haloEnabled; }
        set { _haloEnabled = value;
        if(!value)
		{
			color.a = 0;
			glowMaterial.color = color;
		}
        }
    }

    public void Start()
    {
        player = GameObject.FindGameObjectsWithTag("Player")[0];
		//Renderer r = renderer;
		//if (renderer)
		//{
		//	Material[] materials = new Material[r.materials.Length + 1];
		//	r.materials.CopyTo( materials, 0 );
		//	materials[r.materials.Length] = glowMaterial;
		//	r.materials = materials;
		//}
		glowMaterial = new Material( glowMaterial );
		if(glowChildren)
		{
			Renderer[] children = GetComponentsInChildren<Renderer>();
			foreach ( Renderer i in children )
			{
				Material[] materials = new Material[i.sharedMaterials.Length + 1];
				for ( int j = 0; j < i.sharedMaterials.Length; ++j )
				{
					materials[j] = new Material( i.sharedMaterials[j] );
				}

				materials[i.sharedMaterials.Length] = glowMaterial;
				i.sharedMaterials = materials;
			}
		}
		else
		{
			Material[] materials = new Material[renderer.sharedMaterials.Length + 1];
			for ( int j = 0; j < renderer.sharedMaterials.Length; ++j )
			{
				materials[j] = new Material( renderer.sharedMaterials[j] );
			}

			materials[renderer.sharedMaterials.Length] = glowMaterial;
			renderer.sharedMaterials = materials;
		}
    }


    public void Update()
    {
		bool isClose =  Vector3.Distance(player.transform.position, transform.position) <= playerDistanceBeforeGlow;
		if(!isClose && collider)
		{
			Ray ray = new Ray( player.transform.position, Vector3.Normalize(transform.position - player.transform.position) );
			RaycastHit hitInfo;
			if(Physics.Raycast(ray, out hitInfo, playerDistanceBeforeGlow))
			{
				if(hitInfo.collider.Equals(collider))
				{
					isClose = true;
				}
			}
		}

        if (!HaloEnabled && isClose)
        {
            HaloEnabled = true;
        }
        else if (HaloEnabled && !isClose)
        {
            HaloEnabled = false;
        }

		if(HaloEnabled || triggerGlow)
		{
			currentWave += Time.deltaTime * speed;
			color.a = (Mathf.Cos(currentWave) + 1) / 4;
		}
		else
		{
			color.a = 0;
		}
		glowMaterial.color = color;
    }
}
