﻿using UnityEngine;
using System.Collections;

public class DeadBodyOnTreeEventInvestigatable : UnknownInvestigatable {

	public Spawner leBats;
	public float fallMultiple = 5;
	protected override void Event( GameObject player = null )
	{
		gameObject.GetComponent<SpringJoint>().spring = 0;
		gameObject.GetComponent<Glow>().HaloEnabled = false;
		gameObject.GetComponent<Glow>().enabled = false;
		
		gameObject.rigidbody.AddForce(Physics.gravity * fallMultiple);
	}

	void Update()
	{
		if(gameObject.GetComponent<SpringJoint>().spring == 0) gameObject.rigidbody.AddForce( Physics.gravity * fallMultiple );
	}

	void OnCollisionEnter(Collision collision)
	{
		if(gameObject.GetComponent<SpringJoint>().spring == 0)
		{
			if ( audio ) audio.Play();
			leBats.spawn();
		}
	}
	public override bool canInvestigate( GameObject player = null )
	{
		return gameObject.GetComponent<SpringJoint>().spring != 0;
	}
	public override InvestigatableEnum getInvestigateState( GameObject player = null )
	{
		if(canInvestigate())
		{
			return InvestigatableEnum.Investigate;
		}
		else
		{
			return InvestigatableEnum._;
		}
	}
}
