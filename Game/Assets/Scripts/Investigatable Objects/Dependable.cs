﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Dependable : Investigatable
{
	public Key matchingKey;
	public bool relockable = false;

	public virtual void open() { }

	public override void Investigate( GameObject player = null )
	{
		throw new System.NotImplementedException();
	}

	public override InvestigatableEnum getInvestigateState( GameObject player = null )
	{
		throw new System.NotImplementedException();
	}

	public override bool canInvestigate(GameObject player = null)
	{
		throw new System.NotImplementedException();
	}
}