﻿using System.Collections.Generic;
using UnityEngine;

public class Door : Investigatable
{
	private enum DoorState { None, Open, Close, GhostSlam, Slam }

	protected float lerp = 0;
	public float endRotateAmount;
	public float actionSpeed = 1;
	public GameObject toAnimate;

	private DoorState state = DoorState.None;
	private float waiting;
	private float delay = 0;
	private int ghostSlamWaiting;
	protected Vector3 originRotate;
	protected Vector3 targetRotate;

	public float slamSpeed = 10;
	public float secondsToWaitSlam = 10;
	public int numberOfGhostSlams = 3;
	public float ghostSlamIntervalDelay = 0.5f;
	public Key key = null;
	public DoorUnlockChecker unlocker = new DoorUnlockChecker();
	public AudioClip opened;
	public AudioClip closed;
	public AudioClip opening;
	public AudioClip closing;
	public AudioClip slam;
	public AudioClip ghostSlam;
	public AudioClip locked;
	public AudioSource mainAudio;
	public Spawner spawns;
    public bool isZombieDoor = false;
	public void Open()
	{
		if ( state.Equals(DoorState.Open) || !state.Equals(DoorState.None) || lerp == 1) return;
		state = DoorState.Open;
		if ( mainAudio && opening )
		{
			mainAudio.Stop();
			mainAudio.clip = opening;
			mainAudio.Play();
		}
	}

	public void Close()
	{
		if ( state.Equals( DoorState.Close ) || !state.Equals( DoorState.None ) || lerp == 0 ) return;
		state = DoorState.Close;
		if ( mainAudio && closing )
		{
			mainAudio.Stop();
			mainAudio.clip = closing;
			mainAudio.Play();
		}
	}

	public void Slam()
	{
		if ( state.Equals( DoorState.Slam ) || !state.Equals( DoorState.None ) || lerp == 0 ) return;
		state = DoorState.Slam;
	}

	public new void Start()
	{
		if ( audio ) mainAudio = audio;
		else mainAudio = GetComponentInChildren<AudioSource>();
		if(toAnimate)
		{
			originRotate = toAnimate.transform.localRotation.eulerAngles;
		}
		else
		{
			originRotate = transform.localRotation.eulerAngles;
		}
		targetRotate += new Vector3( 0, endRotateAmount, 0 );
		if ( toAnimate )
		{
			toAnimate.transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, 0.1f ) );
		}
		else
		{
			transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, 0.1f ) );
		}
		if ( toAnimate )
		{
			toAnimate.transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, 0 ) );
		}
		else
		{
			transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, 0 ) );
		}
	}
	private void GhostSlamMovement()
	{
		if ( !mainAudio && !ghostSlam ) state = DoorState.None;
		else if ( !mainAudio.isPlaying)
		{
			delay -= Time.deltaTime;
			if(delay <= 0)
			{
				delay = ghostSlamIntervalDelay;
				--ghostSlamWaiting;
				if ( ghostSlamWaiting > 0 ) state = DoorState.GhostSlam;
				else state = DoorState.None;
				if ( ghostSlam && state.Equals( DoorState.GhostSlam))
				{
					mainAudio.Stop();
					mainAudio.clip = ghostSlam;
					mainAudio.Play();
				}
			}
			if ( toAnimate )
			{
				toAnimate.transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, 0 ) );
			}
			else
			{
				transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, 0 ) );
			}
		}
		else
		{
			float theTime = Random.Range( 0.0f, 0.01f );
			if ( toAnimate )
			{
				toAnimate.transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, theTime ) );
			}
			else
			{
				transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, theTime ) );
			}
		}
	}

	private void SlamMovement()
	{
		if ( lerp > 0 )
		{
			lerp -= Time.deltaTime * slamSpeed;
			if ( lerp <= 0 )
			{
				lerp = 0;
				if ( mainAudio && slam )
				{
					mainAudio.Stop();
					mainAudio.clip = slam;
					mainAudio.Play();
				}
				state = DoorState.None;
			}
		}

		if ( toAnimate )
		{
			//toAnimate.transform.localPosition = Vector3.Lerp( originTranslate, targetTranslate, lerp );
			toAnimate.transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, lerp ) );
		}
		else
		{
			transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, lerp ) );
		}
	}

	private void autoSlam()
	{
		if ( secondsToWaitSlam < 0 ) return;
		if ( lerp <= 0 ) waiting = secondsToWaitSlam;
		waiting -= Time.deltaTime;
		if(waiting <= 0) state = DoorState.Slam;
	}

	// Update is called once per frame 
	public override void Update ()
	{
		autoSlam();
		if ( state.Equals( DoorState.None ) ) return;
		if ( state.Equals(DoorState.Slam )) SlamMovement();
		else if ( state.Equals(DoorState.GhostSlam )) GhostSlamMovement();
		else NormalMovement();
	}
	private void NormalMovement()
	{
		if ( state.Equals(DoorState.Open) && lerp < 1 )
		{
			lerp += Time.deltaTime * actionSpeed;
			if ( lerp >= 1 )
			{
				lerp = 1;
				if ( mainAudio && !isZombieDoor)
				{
                    mainAudio.Stop();
					if(opened)
					{
						mainAudio.clip = opened;
						mainAudio.Play();
					}
				}
				state = DoorState.None;
			}
		}
		else if ( state.Equals(DoorState.Close) && lerp > 0 )
		{
			lerp -= Time.deltaTime * actionSpeed;
			if ( lerp <= 0 )
			{
				lerp = 0;
				if ( mainAudio && closed )
				{
					mainAudio.Stop();
					mainAudio.clip = closed;
					mainAudio.Play( );
				}
				state = DoorState.None;
			}
		}
		if ( toAnimate )
		{
			toAnimate.transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, lerp ) );
		}
		else
		{
			transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, lerp ) );
		}
	}

	public override void Investigate( GameObject player = null )
	{
		if ( !state.Equals( DoorState.None ) ) return;
		Investigated = true;
		if ( lerp >= 1 )
		{
			Close();
		}
		else if(lerp <= 0)
		{
			if(key)
			{
				InventoryContainer playersStuff = player.GetComponent<FirstPersonDrifter>().inventory;
				IList<Collectable> theKeys = playersStuff.GetItems<Key>();
				bool found = false;
				foreach ( Collectable i in theKeys )
				{
					if ( i.Equals( key ) )
					{
						found = true;
						break;
					}
				}

				if ( found )
				{
					Open();
					if ( spawns ) spawns.spawn();
				}
				else
				{
					if ( mainAudio && locked )
					{
						mainAudio.Stop();
						mainAudio.clip = locked;
						mainAudio.Play();
					}
					if ( Random.value < 0.25 )
					{
						state = DoorState.GhostSlam;
						ghostSlamWaiting = numberOfGhostSlams;
						mainAudio.Stop();
						mainAudio.clip = ghostSlam;
						mainAudio.Play();
					}
				}
			}
			else if ( unlocker )
			{
				if(unlocker.canOpen(player))
				{
					Open();
					if ( spawns ) spawns.spawn();
				}
				else
				{
					if ( mainAudio && locked )
					{
						mainAudio.Stop();
						mainAudio.clip = locked;
						mainAudio.Play( );
					}
					if(Random.value < 0.25)
					{
						state = DoorState.GhostSlam;
						ghostSlamWaiting = numberOfGhostSlams;
						mainAudio.Stop();
						mainAudio.clip = ghostSlam;
						mainAudio.Play();
					}
				}
			}
			else
			{
				Open();
				if ( spawns ) spawns.spawn();
			}
		}
	}

	public override InvestigatableEnum getInvestigateState(GameObject player = null)
	{
		if ( !state.Equals( DoorState.None ) ) return InvestigatableEnum._;
		else if ( lerp <= 0 )
		{
			if(key)
			{
				InventoryContainer playersStuff = player.GetComponent<FirstPersonDrifter>().inventory;
				IList<Collectable> theKeys = playersStuff.GetItems<Key>();
				bool found = false;
				foreach ( Collectable i in theKeys )
				{
					if ( i.Equals( key ) )
					{
						found = true;
						break;
					}
				}
				if ( found || !Investigated )
				{
					return InvestigatableEnum.Open;
				}
				else
				{
					return InvestigatableEnum.Locked;
				}
			}
			else if(unlocker)
			{
				if ( unlocker.canOpen( player ) || !Investigated )
				{
					return InvestigatableEnum.Open;
				}
				else
				{
					return InvestigatableEnum.Locked;
				}
			}
			else
			{
				return InvestigatableEnum.Open;
			}
		}
		else if ( lerp >= 1 ) return InvestigatableEnum.Close;
		else return InvestigatableEnum._;
	}

	public override bool canInvestigate(GameObject player = null)
	{
		return ( state.Equals( DoorState.None ) );
	}

    public bool isOpen()
    {
        return (state == DoorState.Open);
    }
}