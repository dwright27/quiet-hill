﻿using UnityEngine;

public class DoorUnlockChecker : Investigatable
{

	public virtual bool canOpen( GameObject player = null )
	{
		return true;
	}

	public override void Investigate( GameObject player = null )
	{

	}

	public override InvestigatableEnum getInvestigateState( GameObject player = null )
	{
		return InvestigatableEnum._;
	}

	public override bool canInvestigate(GameObject player = null)
	{
		return false;
	}
	public override void Update()
	{
		//base.Update();
	}
}
