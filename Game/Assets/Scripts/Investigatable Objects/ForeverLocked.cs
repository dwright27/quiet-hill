﻿using UnityEngine;
using System.Collections;

public class ForeverLocked : DoorUnlockChecker
{
	public override bool canOpen( GameObject player )
	{
		return false;
	}
}
