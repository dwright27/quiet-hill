﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Generator : Dependable
{
	public AudioClip openedSound;
	private bool openedBefore = false;
	public override void Investigate( GameObject player = null )
	{
		if ( matchingKey )
		{
			InventoryContainer playersStuff = player.GetComponent<FirstPersonDrifter>().inventory;
			IList<Collectable> theKeys = playersStuff.GetItems<Key>();
			foreach ( Collectable i in theKeys )
			{
				if ( i.Use( gameObject ) )
				{
					openedBefore = true;
					open();
					return;
				}
			}
		}
		else
		{
			open();
		}
	}

	public override InvestigatableEnum getInvestigateState( GameObject player = null )
	{
		if(!openedBefore && matchingKey)
		{
			InventoryContainer playersStuff = player.GetComponent<FirstPersonDrifter>().inventory;
			IList<Collectable> theKeys = playersStuff.GetItems<Key>();
			foreach ( Collectable i in theKeys )
			{
				if ( i is Key && (i as Key).Equals(matchingKey) )
				{
					return InvestigatableEnum.Use;
				}
			}
		}
		return InvestigatableEnum._;
		
	}

	public override bool canInvestigate(GameObject player = null)
	{
		return true;
	}

	public override void open()
	{
		if(audio && openedSound)
		{
			audio.clip = openedSound;
			audio.Play();
		}
		if ( GetComponent<Spawner>() ) GetComponent<Spawner>().spawn();
	}
}
