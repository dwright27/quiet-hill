﻿using UnityEngine;
public abstract class Investigatable : MonoBehaviour
{

    private GameObject player;
	public bool Investigated { get; set; }
    public Investigatable()
    {
       
    }

	public abstract void Investigate(GameObject player = null);
	public abstract InvestigatableEnum getInvestigateState(GameObject player = null);
	public abstract bool canInvestigate( GameObject player = null );

    public virtual void Update()
    {
		if(GetComponent<Glow>())
		{
			bool investigatable = canInvestigate();
			GetComponent<Glow>().enabled = investigatable;
			if(!investigatable) GetComponent<Glow>().HaloEnabled = false;
		}
		else if(GetComponentInChildren<Glow>())
		{
			bool investigatable = canInvestigate();
			GetComponentInChildren<Glow>().enabled = investigatable;
			if ( !investigatable ) GetComponentInChildren<Glow>().HaloEnabled = false;
		}
    }

}
