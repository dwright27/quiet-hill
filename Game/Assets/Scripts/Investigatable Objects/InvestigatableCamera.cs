﻿using UnityEngine;
using System.Collections;

public class InvestigatableCamera : Investigatable
{
	private GameObject target = null;
	private bool holding = false;
	void Start()
	{
		if(camera)
		{
			camera.enabled = false;
		}
		else if(GetComponentInChildren<Camera>())
		{
			GetComponentInChildren<Camera>().enabled = false;
		}
		if(light)
		{
			light.enabled = false;
		}
		else if(GetComponentInChildren<Light>())
		{
			GetComponentInChildren<Light>().enabled = false;
		}
	}

	void LateUpdate()
	{
		if(Input.GetMouseButtonDown(0) && target && !holding)
		{
			if ( camera )
			{
				camera.enabled = false;
			}
			else if ( GetComponentInChildren<Camera>() )
			{
				GetComponentInChildren<Camera>().enabled = false;
			}
			if ( light )
			{
				light.enabled = false;
			}
			else if ( GetComponentInChildren<Light>() )
			{
				GetComponentInChildren<Light>().enabled = false;
			}
			if ( target.light )
			{
				target.light.enabled = true;
			}
			else if ( target.GetComponentInChildren<Light>() )
			{
				target.GetComponentInChildren<Light>().enabled = true;
			}
			target.GetComponent<FirstPersonDrifter>().EnablePlayerControl();
			target = null;
		}
		else if(!Input.GetMouseButtonDown(0))
		{
			holding = false;
		}
	}

	public override void Investigate( GameObject player = null )
	{
		target = player;
		player.GetComponent<FirstPersonDrifter>().DisablePlayerControl();
		if(player.light)
		{
			player.light.enabled = false;
		}
		else if ( player.GetComponentInChildren<Light>())
		{
			player.GetComponentInChildren<Light>().enabled = false;
		}
		if ( camera )
		{
			camera.enabled = true;
		}
		else if ( GetComponentInChildren<Camera>() )
		{
			GetComponentInChildren<Camera>().enabled = true;
		}
		if ( light )
		{
			light.enabled = true;
		}
		else if ( GetComponentInChildren<Light>() )
		{
			GetComponentInChildren<Light>().enabled = true;
		}
		holding = true;
	}

	public override InvestigatableEnum getInvestigateState( GameObject player = null )
	{
		return InvestigatableEnum.Investigate;
	}

	public override bool canInvestigate(GameObject player = null)
	{
		return true;
	}
}
