﻿using UnityEngine;
public class InvestigatableHiding : Investigatable
{
	private bool inThisObject = false;
	private bool mouseBuffer = false;
	private GameObject target;
	public Vector3 positionOffset;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	new void Update ()
	{
		if ( !mouseBuffer && Input.GetMouseButton(0) )
		{
			inThisObject = false;
			mouseBuffer = true;
		}
		if(mouseBuffer && !Input.GetMouseButton(0))
		{
			mouseBuffer = false;
		}
		if(inThisObject && target)
		{
			target.SetActive( false );

			if(camera)
			{
				camera.enabled = true;
			}
			else if(GetComponentInChildren<Camera>())
			{
				GetComponentInChildren<Camera>().enabled = true;
			}
			if(GetComponent<AudioListener>())
			{
				GetComponent<AudioListener>().enabled = true;
			}
			else if(GetComponentInChildren<AudioListener>())
			{
				GetComponentInChildren<AudioListener>().enabled = true;
			}
			if(light)
			{
				light.enabled = true;
			}
			else if(GetComponentInChildren<Light>())
			{
				GetComponentInChildren<Light>().enabled = true;
			}
		}
		else if(!inThisObject && target && !target.activeSelf)
		{
			target.SetActive( true );

			if ( camera )
			{
				camera.enabled = false;
			}
			else if ( GetComponentInChildren<Camera>() )
			{
				GetComponentInChildren<Camera>().enabled = false;
			}
			if ( GetComponent<AudioListener>() )
			{
				GetComponent<AudioListener>().enabled = false;
			}
			else if ( GetComponentInChildren<AudioListener>() )
			{
				GetComponentInChildren<AudioListener>().enabled = false;
			}
			if ( light )
			{
				light.enabled = false;
			}
			else if ( GetComponentInChildren<Light>() )
			{
				GetComponentInChildren<Light>().enabled = false;
			}
            target.GetComponent<FirstPersonDrifter>().IsHidden = false;

			target = null;
		}
	}

	public override void Investigate(GameObject player = null)
	{
		target = player;
        player.GetComponent<FirstPersonDrifter>().IsHidden = true;
		inThisObject = inThisObject == false;
		mouseBuffer = true;
	}

	public override InvestigatableEnum getInvestigateState(GameObject player = null)
	{
		return InvestigatableEnum.Hide;
	}

	public override bool canInvestigate(GameObject player = null)
	{
		return true;
	}
}
