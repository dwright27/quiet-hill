﻿using UnityEngine;
using System.Collections;

public class InvestigateAnimate : Investigatable {

	protected InvestigatableEnum state;
	protected bool toOpen = false;
	protected float lerp = 0;
	protected Vector3 originTranslate;
	protected Vector3 originRotate;
	protected Vector3 originScale = new Vector3(1,1,1);
	public Vector3 targetTranslate;
	public Vector3 targetRotate;
	public Vector3 targetScale;
	public float actionSpeed = 1;
	public GameObject toAnimate;

	// Use this for initialization
	public void Start ()
	{
		if(toAnimate)
		{
			originTranslate = toAnimate.transform.localPosition;
			targetTranslate += originTranslate;
			originRotate = toAnimate.transform.eulerAngles;
			targetRotate += originRotate;
			originScale = toAnimate.transform.localScale;
			targetScale += originScale;
		}
		else
		{
			originTranslate = transform.localPosition;
			targetTranslate += originTranslate;
			originRotate = transform.eulerAngles;
			targetRotate += originRotate;
			originScale = transform.localScale;
			targetScale += originScale;
		}
	}
	
	// Update is called once per frame
	public new void Update ()
	{
		if(toOpen && lerp < 1)
		{
			lerp += Time.deltaTime * actionSpeed;
			if ( lerp > 1 )
			{
				lerp = 1;
			}
			if ( toAnimate )
			{
				toAnimate.transform.localPosition = Vector3.Lerp( originTranslate, targetTranslate, lerp );
				toAnimate.transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, lerp ) );
				toAnimate.transform.localScale = Vector3.Lerp( originScale, targetScale, lerp );
			}
			else
			{
				transform.localPosition = Vector3.Lerp( originTranslate, targetTranslate, lerp );
				transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, lerp ) );
				transform.localScale = Vector3.Lerp( originScale, targetScale, lerp );
			}
		}
		else if(!toOpen && lerp > 0)
		{
			lerp -= Time.deltaTime * actionSpeed;
			if ( lerp < 0 )
			{
				lerp = 0;
			}
			if ( toAnimate )
			{
				toAnimate.transform.localPosition = Vector3.Lerp( originTranslate, targetTranslate, lerp );
				toAnimate.transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, lerp ) );
				toAnimate.transform.localScale = Vector3.Lerp( originScale, targetScale, lerp );
			}
			else
			{
				transform.localPosition = Vector3.Lerp( originTranslate, targetTranslate, lerp );
				transform.localRotation = Quaternion.Euler( Vector3.Lerp( originRotate, targetRotate, lerp ) );
				transform.localScale = Vector3.Lerp( originScale, targetScale, lerp );
			}
		}
	}

	public override void Investigate(GameObject player = null)
	{
		if (lerp <= 0 || lerp >= 1 ) toOpen = !toOpen;
	}


	public override InvestigatableEnum getInvestigateState(GameObject player = null)
	{
		if ( lerp <= 0 ) return InvestigatableEnum.Open;
		else if ( lerp >= 1 ) return InvestigatableEnum.Close;
		else return InvestigatableEnum._;
	}

	public override bool canInvestigate(GameObject player = null)
	{
		return (lerp <= 0 && !toOpen) || (lerp >= 1 && toOpen);
	}
}