﻿using UnityEngine;
using System.Collections;

public class InvestigateHold : Investigatable {

	// Use this for initialization

    public bool holding;
    private InvestigatableEnum currentState = InvestigatableEnum.Pick_Up;


	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

    public override void Investigate(GameObject player = null)
    {
        FirstPersonDrifter p = player.GetComponent<FirstPersonDrifter>();
        p.HoldItem(gameObject);
      //  rigidbody.useGravity = false;
        //rigidbody.isKinematic = true;
       // rigidbody.detectCollisions = true;
       // holding = true;
    }

    public override InvestigatableEnum getInvestigateState(GameObject player = null)
    {
        return currentState;
    }


    public void Disable()
    {
        holding = false;
        currentState =  InvestigatableEnum._;
    }

    public void Enable()
    {
        holding = true;
        currentState = InvestigatableEnum.Pick_Up;
    }

    public override bool canInvestigate(GameObject player = null)
    {
        return !holding;
    }
}
