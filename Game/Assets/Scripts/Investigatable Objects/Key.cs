﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

	public class Key : Collectable, IEquatable<Key>
	{
		private static int _nextKeyID = 0;
		public int id{get;set;}
		
		public override bool Use( GameObject obj = null )
		{
			if ( oneTimeUse && used ) return false;
			else
			{
				used = true;
				return true;
			}
		}

		public bool Equals( Key other )
		{
			return id.Equals(other.id);
		}

		public void Start()
		{
			id = _nextKeyID++;
		}

	}