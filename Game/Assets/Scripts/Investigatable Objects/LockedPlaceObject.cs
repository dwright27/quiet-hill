﻿using UnityEngine;
using System.Collections;

public class LockedPlaceObject :  PlaceObjectKey{


   
	// Use this for initialization
	void Start () {
	
	}

    public override bool canInvestigate(GameObject player = null)
    {
        return !keyPlaced;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public override InvestigatableEnum getInvestigateState(GameObject player = null)
    {
        return InvestigatableEnum.Locked;
    }


}
