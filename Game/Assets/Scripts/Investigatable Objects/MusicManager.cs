﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour
{
	private AudioSource[] musics;
	private AudioSource target;
	public float fadeSpeed = float.MaxValue;
	// Use this for initialization
	void Start ()
	{
		musics = GetComponentsInChildren<AudioSource>();
		foreach(AudioSource i in musics)
		{
			i.volume = 0;
			i.Stop();
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		foreach(AudioSource i in musics)
		{
			if(i.Equals(target))
			{
				i.volume += fadeSpeed * Time.deltaTime;
			}
			else
			{
				i.volume -= fadeSpeed * Time.deltaTime;
				if ( i.volume <= 0 ) i.Stop();
			}
		}
	}

	public bool setTarget(int id)
	{
		if ( id < 0 || id >= musics.Length )
		{
			target = null;
		}
		else
		{
			target = musics[id];
			if ( !target.isPlaying ) target.Play();
		}
		return target != null;
	}
}
