﻿using UnityEngine;
using System.Collections;

public class PentagramPuzzle : Investigatable
{
	private int selectedCandle = -1;
	private Vector3 lastPosition, otherLastPosition, otherLastPosition2;
	private GameObject animatingCandle;
	private float lerp, otherLerp = 0, otherLerp2 = 0;
	private GameObject player;
	public GameObject normalCandle, normalCandle2;
	public GameObject puzzleCamera;
	public Renderer[] candles;
	public Collider[] selectionTriggers;
	public LockMouse theMouseLocker;
	public int[] candleLocations;
	public int[] AnswerSpots;
	public float moveSpeed = 1;
	public float otherSpeed = 0.1f;

	public bool Solved { get; private set; }
	public void SolvePentaPuzzle()
	{
		if ( theMouseLocker ) theMouseLocker.enabled = false;
		Screen.lockCursor = false;
		puzzleCamera.gameObject.SetActive( true );
		if ( player ) player.SetActive( false );
		if ( GetComponent<Glow>() )
		{
			GetComponent<Glow>().enabled = false;
			GetComponent<Glow>().HaloEnabled = false;
		}
		if(GetComponent<Collider>())
		{
			GetComponent<Collider>().enabled = false;
		}
	}
	public void StopSolvingPentaPuzzle()
	{
		if ( theMouseLocker ) theMouseLocker.enabled = true;
		Screen.lockCursor = true;
		puzzleCamera.gameObject.SetActive( false );
		if ( player ) player.SetActive( true );
		player = null;
		if ( GetComponent<Glow>() )
		{
			GetComponent<Glow>().enabled = true;
			GetComponent<Glow>().HaloEnabled = true;
		}
		if ( GetComponent<Collider>() )
		{
			GetComponent<Collider>().enabled = true;
		}
	}
	// Use this for initialization
	void Start ()
	{
		for ( int i = 0; i < candleLocations.Length; ++i )
		{
			candles[i].transform.position = selectionTriggers[candleLocations[i]].transform.position;
		}
		otherLastPosition = normalCandle.transform.localPosition;
		otherLastPosition2 = normalCandle2.transform.localPosition;
	}

	void AnimateOtherCandles()
	{
		if ( otherLerp == 0 && normalCandle.audio ) normalCandle.audio.Play();
		otherLerp += Time.deltaTime * otherSpeed;
		if ( otherLerp >= 1 )
		{
			if ( !normalCandle.GetComponentInChildren<ParticleEmitter>().emit && normalCandle.GetComponentInChildren<ParticleEmitter>().audio ) normalCandle.GetComponentInChildren<ParticleEmitter>().audio.Play();
			normalCandle.GetComponentInChildren<ParticleEmitter>().emit = true;
			otherLerp = 1;
			
		}
		normalCandle.transform.localPosition = Vector3.Lerp( otherLastPosition, Vector3.zero, otherLerp );
		if(otherLerp == 1)
		{
			if ( otherLerp2 == 0 && normalCandle2.audio ) normalCandle2.audio.Play(); 
			otherLerp2 += Time.deltaTime * otherSpeed;
			if ( otherLerp2 >= 1 )
			{
				if ( !normalCandle2.GetComponentInChildren<ParticleEmitter>().emit )
				{
					if ( normalCandle2.GetComponentInChildren<ParticleEmitter>().audio ) normalCandle2.GetComponentInChildren<ParticleEmitter>().audio.Play();
					if ( audio ) audio.Play();
				}
				normalCandle2.GetComponentInChildren<ParticleEmitter>().emit = true;
				otherLerp2 = 1;
			}
			normalCandle2.transform.localPosition = Vector3.Lerp( otherLastPosition2, Vector3.zero, otherLerp2 );
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if ( puzzleCamera.activeSelf && Input.GetKeyUp( KeyCode.Escape ) ) StopSolvingPentaPuzzle();
		for ( int i = 0; i < selectionTriggers.Length; ++i )
		{
			selectionTriggers[i].gameObject.SetActive( false );
		}
		for ( int i = 0; i < candleLocations.Length; ++i )
		{
			if ( candleLocations[i] == AnswerSpots[i] && candles[i].gameObject != animatingCandle )
			{
				if ( !candles[i].GetComponentInChildren<ParticleEmitter>().emit && candles[i].GetComponentInChildren<ParticleEmitter>().audio ) candles[i].GetComponentInChildren<ParticleEmitter>().audio.Play();
				candles[i].GetComponentInChildren<ParticleEmitter>().emit = true;
			}
			else
			{
				candles[i].GetComponentInChildren<ParticleEmitter>().emit = false;
			}
		}
		if(Solved && !animatingCandle)
		{
			AnimateOtherCandles();
		}
		if(Solved)
		{
			GetComponent<Glow>().HaloEnabled = false;
			GetComponent<Glow>().enabled = false;
		}
		if ( ( !animatingCandle ) && ( Solved || !puzzleCamera.gameObject.activeSelf ) )
		{
			return;
		}
		bool allSolved = true;
		for ( int i = 0; i < AnswerSpots.Length && allSolved;++i )
		{
			if ( AnswerSpots[i] != candleLocations[i] ) allSolved = false;
		}
		Solved = allSolved;

			if ( animatingCandle )
			{
				if(lerp == 0 && animatingCandle.audio) animatingCandle.audio.Play();
				lerp += Time.deltaTime * moveSpeed;
				if ( lerp >= 1 ) lerp = 1;
				int i =0;
				for ( i = 0; i < candles.Length; ++i )
				{
					if(candles[i].gameObject.Equals(animatingCandle))
					{
						break;
					}
				}
				if(i < candles.Length) animatingCandle.transform.position = Vector3.Lerp( lastPosition, selectionTriggers[candleLocations[i]].transform.position, lerp );
				if ( lerp == 1 )
				{
					if ( animatingCandle.audio ) animatingCandle.audio.Stop();
					animatingCandle = null;
				}
				return;
			}
		if(selectedCandle < 0)
		{
			for ( int i = 0; i < selectionTriggers.Length; ++i )
			{
				bool isCandleLoc = false;
				for ( int j = 0; j < candleLocations.Length && !isCandleLoc; ++j )
				{
					if ( i == candleLocations[j] )
					{
						isCandleLoc = true;
						break;
					}
				}
				if ( isCandleLoc )
				{
					//light halo
					selectionTriggers[i].gameObject.SetActive( true );
				}
				else
				{
					//lightHalo
					selectionTriggers[i].gameObject.SetActive( false );
				}
			}
		}
		else
		{
			int selectable = ( (( candleLocations[selectedCandle]  + selectionTriggers.Length) + 1) % selectionTriggers.Length );
			int selectable2 = ( (( candleLocations[selectedCandle]  + selectionTriggers.Length) - 1) % selectionTriggers.Length );
			for ( int i = 0; i < selectionTriggers.Length; ++i )
			{
				if(i == selectable || i == selectable2)
				{
					bool isCandleLoc = false;
					for ( int j = 0; j < candleLocations.Length && !isCandleLoc; j++ )
					{
						if(candleLocations[j] == i)
						{
							isCandleLoc = true;
							break;
						}
					}
						if (!isCandleLoc)
						{
							selectionTriggers[i].gameObject.SetActive( true );
						}
						else selectionTriggers[i].gameObject.SetActive( false );
				}
				//lightHalo
				else selectionTriggers[i].gameObject.SetActive( false );
			}
			selectionTriggers[candleLocations[selectedCandle]].gameObject.SetActive( true );
		}

		if( Input.GetMouseButtonUp( 0 ))
		{
			RaycastHit hitInfo;
			var ray = puzzleCamera.camera.ScreenPointToRay( Input.mousePosition );
			
			if(Physics.Raycast(ray, out hitInfo, 1000))
			{
				int i = 0;
				Collider item = hitInfo.collider;
				for(i = 0; i < selectionTriggers.Length; ++i)
				{
					if(selectionTriggers[i].Equals(item))
					{
						break;
					}
				}
				if ( i >= selectionTriggers.Length ) return;
				if(selectedCandle < 0)
				{
					int j = 0;
					for ( j = 0; j < candleLocations.Length; ++j )
					{
						if(candleLocations[j].Equals(i))
						{
							break;
						}
					}
					selectedCandle = j;
				}
				else
				{
					if ( candleLocations[selectedCandle] != i )
					{
						candleLocations[selectedCandle] = i;
						animatingCandle = candles[selectedCandle].gameObject;
						lastPosition = animatingCandle.transform.position;
						lerp = 0;
					}
					selectedCandle = -1;
				}
			}
		}
	}

	public override void Investigate( GameObject player = null )
	{
		this.player = player;
		SolvePentaPuzzle();
	}

	public override InvestigatableEnum getInvestigateState( GameObject player = null )
	{
		if ( Solved ) return InvestigatableEnum._;
		else return InvestigatableEnum.Investigate;
	}

	public override bool canInvestigate( GameObject player = null )
	{
		return !Solved;
	}
}