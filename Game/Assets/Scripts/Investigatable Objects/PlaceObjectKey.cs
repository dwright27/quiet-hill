﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlaceObjectKey : DoorUnlockChecker
{
	protected bool keyPlaced = false;
    public AudioClip unlockSound;
    public GameObject door;
	public Spawner objectsToPlace;
	public Key theKey;
	public bool disableCollider = false;
    private bool playedSound = false;

	public override bool canOpen( GameObject player = null )
	{

		return keyPlaced;
	}

	public override void Update()
	{
		//base.Update();
	}

	public override void Investigate( GameObject player = null )
	{
		if ( keyPlaced ) return;
		InventoryContainer playersStuff = player.GetComponent<FirstPersonDrifter>().inventory;
		IList<Collectable> theKeys = playersStuff.GetItems<Key>();
		bool found = false;
		foreach ( Collectable i in theKeys )
		{
			if ( i.Equals( theKey ) )
			{
				found = true;
				break;
			}
		}
        if (found)
        {
            if (objectsToPlace) objectsToPlace.spawn();
			//if ( GetComponent<Glow>() )
			//{
			//	GetComponent<Glow>().HaloEnabled = false;
			//	GetComponent<Glow>().enabled = false;
			//}
            keyPlaced = true;
            if(audio && unlockSound) audio.PlayOneShot(unlockSound);

            if (door != null)
            {
                door.GetComponentInChildren<Door>().Open();
            }
			if ( disableCollider && collider )
			{
				collider.enabled = false;
			}
        }
	}

	public override InvestigatableEnum getInvestigateState( GameObject player = null )
	{
		if ( keyPlaced ) return InvestigatableEnum._;
		InventoryContainer playersStuff = player.GetComponent<FirstPersonDrifter>().inventory;
		IList<Collectable> theKeys = playersStuff.GetItems<Key>();
		bool found = false;
		foreach ( Collectable i in theKeys )
		{
			if ( i.Equals( theKey ) )
			{
				found = true;
				break;
			}
		}
		if ( found )
		{
			return InvestigatableEnum.Place;
		}
		return InvestigatableEnum.Needs_Something;
	}

	public override bool canInvestigate(GameObject player = null)
	{
		return !getInvestigateState(player).Equals(InvestigatableEnum._);
	}
}
