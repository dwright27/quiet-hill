﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Portrait : InvestigateAnimate
{
    public AudioClip openedSound;
	public DoorUnlockChecker dependable;
    private float timer = .0f;
    private float delay = 2.0f;
    private bool startCounting = false;

    public override void Investigate(GameObject player = null)
    {
		if(dependable) dependable.Investigate(player);
		if ( dependable && !dependable.canOpen(player) ) return;
		else
		{
            startCounting = true;
			if ( audio && openedSound )
			{
				audio.clip = openedSound;
				audio.Play();
			}


			if ( GetComponent<Spawner>() ) GetComponent<Spawner>().spawn();
		}
    }

    void Update()
    {
        base.Update();

        Open();
    }

    void Open()
    {
        if (timer < delay && startCounting)
        {
            timer += Time.deltaTime;
        }
        else if(timer > delay)
        {
            toOpen = true;
        }

    }


    public override InvestigatableEnum getInvestigateState(GameObject player = null)
    {
		if ( dependable )
		{
			return dependable.getInvestigateState( player );
		}
		else
		{
			return InvestigatableEnum.Open;
		}
    }

    public override bool canInvestigate(GameObject player = null)
    {
		bool toreturn = true;
		if ( dependable )toreturn = dependable.canInvestigate( player );
		if ( GetComponent<Glow>() )
		{
			GetComponent<Glow>().enabled = toreturn;
			if ( !toreturn ) GetComponent<Glow>().HaloEnabled = false;
		}
		else if ( GetComponentInChildren<Glow>() )
		{
			GetComponentInChildren<Glow>().enabled = toreturn;
			if ( !toreturn ) GetComponentInChildren<Glow>().HaloEnabled = false;
		}
		return toreturn;
    }
}
