﻿using UnityEngine;
using System.Collections;

public class RatInvestigate : UnknownInvestigatable {

	// Use this for initialization
    public int numberOfRats = 5;
    public GameObject ratPrefrab;
    private bool investigated = false;

    public float delay = 0;
    private float timer = 0;
    private bool delayStarted = false;
    private GameObject jenkPlayerHolder;//so delay can work with base event 

    protected override void Event(GameObject player = null)
    {
        if (!investigated)
        {
            jenkPlayerHolder = player;
            delayStarted = true;
            investigated = true;
        }
    }

	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if (delayStarted)
        {
            timer += Time.deltaTime;
            if (timer > delay)
            {
                base.Event(jenkPlayerHolder);
                TriggerMice();
                delayStarted = false;
            }
        }
	}


    private void TriggerMice()
    {
        for(int i = 0; i < numberOfRats; i++)
        {
            Instantiate(ratPrefrab,new Vector3(gameObject.transform.position.x, 1.0f,gameObject.transform.position.z), ratPrefrab.transform.rotation);
        }
    }

    public bool wasinvestigated()
    {
        return investigated;
    }

	public override void Investigate( GameObject player = null )
	{
		if ( !investigated )
		{
			jenkPlayerHolder = player;
			delayStarted = true;
			investigated = true;
			if ( GetComponent<Glow>() )
			{
				GetComponent<Glow>().HaloEnabled = false;
				GetComponent<Glow>().enabled = false;
			}
		}
	}

	public override InvestigatableEnum getInvestigateState( GameObject player = null )
	{
		if ( !investigated ) return InvestigatableEnum.Investigate;
		else return InvestigatableEnum._;
	}

	public override bool canInvestigate( GameObject player = null )
	{
		return !investigated;
	}
}
