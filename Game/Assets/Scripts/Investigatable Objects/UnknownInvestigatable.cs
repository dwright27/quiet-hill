﻿using UnityEngine;
using System.Collections;

public class UnknownInvestigatable : Investigatable
{
	//do event stuff
	protected virtual void Event( GameObject player = null ) { }
	public override void Investigate( GameObject player = null )
	{
		Event( player );
	}

	public override InvestigatableEnum getInvestigateState( GameObject player = null )
	{
		return InvestigatableEnum.Investigate;
	}

	public override bool canInvestigate( GameObject player = null )
	{
		return true;
	}
}
