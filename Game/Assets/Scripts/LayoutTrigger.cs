﻿using UnityEngine;
using System.Collections;

public class LayoutTrigger : MonoBehaviour {
    public GameObject obj1;
    public GameObject obj2;
    public GameObject obj3;
    public GameObject obj4;

    public Vector3 obj1ChangedPosition;
    public Vector3 obj2ChangedPosition;
    public Vector3 obj3ChangedPosition;
    public Vector3 obj4ChangedPosition;

    public bool isActive = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (isActive && other.GetComponent<FirstPersonDrifter>() != null)
        {
            Debug.Log("layout changed");
            if (obj1 != null)
            {
                obj1.gameObject.transform.position = obj1ChangedPosition;
            }

            if (obj2 != null)
            {
                obj2.gameObject.transform.position = obj2ChangedPosition;
            }

            if (obj3 != null)
            {
                obj3.gameObject.transform.position = obj3ChangedPosition;
            }

            if (obj4 != null)
            {
                obj4.gameObject.transform.position = obj4ChangedPosition;
            }
        }
    }
}
