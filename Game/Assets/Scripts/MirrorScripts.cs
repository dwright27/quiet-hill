﻿using UnityEngine;
using System.Collections;

public class MirrorScripts : MonoBehaviour {

    public GameObject player;
    public float rotationScale;
    public Camera renderCam;
    public float normalAxisXRotationAngleAroundY;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        var reflect =  (renderCam.transform.position - player.transform.position).normalized;
        //var rot = mirrorPlane.transform.rotation;
        //var normal = new Vector3(-rot.x, 0.0f, 0.0f).normalized;
        //Debug.Log(normal.ToString());
        var xBaseAxis = new Vector3(1.0f, 0.0f, 0.0f);
        var normal = (Quaternion.AngleAxis(normalAxisXRotationAngleAroundY, new Vector3(0.0f, 1.0f, 0.0f)) * xBaseAxis).normalized;

        reflect = Vector3.Reflect(reflect, normal) * rotationScale;
        //Debug.Log(reflect);
        renderCam.gameObject.transform.LookAt(reflect + renderCam.gameObject.transform.position); //Quaternion.LookRotation(reflect + renderCam.transform.position);
	}

    //void OnPreRender()
    //{
    //    player.renderer.enabled = false;
    //    ghost.renderer.enabled = false;
       
    //}

    //void OnPostRender()
    //{
    //    player.renderer.enabled = false;
    //    ghost.renderer.enabled = true;
    //}
}
