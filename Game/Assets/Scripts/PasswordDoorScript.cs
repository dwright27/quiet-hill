﻿using UnityEngine;
using System.Collections;

public class PasswordDoorScript : MonoBehaviour {

	// Use this for initialization

    public bool puzzle1 = false;
    public bool puzzle2 = false;
    public string password = "1234";
    private string passwordGuess = "";
    public Door door;
    public GameObject player;
    public bool unlocked = false;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}



    void OnGUI()
    {
        if (!unlocked && (Vector3.Distance(this.gameObject.transform.position, player.transform.position) < 15))
        {
            passwordGuess = GUI.TextField(new Rect(Screen.width * 0.5f, Screen.height * 0.3f, 100, 50), passwordGuess);
            if(passwordGuess.Length == password.Length)
            {
                if (puzzle1 && puzzle2 && password.Equals(passwordGuess))
                {
                    door.Open();
                    unlocked = true;
                }
                else
                {
                    passwordGuess = "";
                }
            }
        }
    }
}
