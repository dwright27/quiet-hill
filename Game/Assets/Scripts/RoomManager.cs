﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

//Must have references to the objects that will be thrown (scary objects)
    //these objects must have rigidbody components (set the attributes accordingly)
//RoomManager doesn't have references to triggers, but knows how many are active each frame (Max number of objects that should be thrown)

//when the player collides with a ScaryTrigger (multi) an event tries to occur
//the event can occur if the time since the last event is more than the time limit between events
//when event occurs:
    //throws the object in a random direction relative to the gameobject's rotation axis (with a force up to the maxForceValue)
    //multiple throws can happen each frame if the roomManager receives signals from the scary triggers at the same time
    //signals are sent after the individual delay time of each trigger
namespace Assets.Scripts
{
    public class RoomManager : MonoBehaviour
    {
        private float eventTimer = 0;
        private int numTriggersActiveThisFrame = 0;
        private int currObjectIndex = 0;
        private float maxComponentValue;

        public float timeLimit = 180.0f;
        public float maxForceValue = 5000f;
        public List<GameObject> scaryObjects;

        //adds a relative force to the referenced gameobject up to the maxForceValue
        public void applyThrowForceToScaryObject(int i)
        {
            float x, y, z;
            if (i > scaryObjects.Count())
            {
                //trying to throu
                Debug.Log("out of bounds");
                throw new Exception();
            }

            //length of a vector is defined as sqrt(x*x + y*y +z*z) = value
            //value^2 = x*x + y*y + z*z
            //assuming all three components can have an equal maxForce in their direction
            //value^2 = x*x + x*x + x*x
            //value^2 = 3 * x^2
            //(value^2)/3 = x^2
            //sqrt(value^2/3) = x = maxComponentValue
            maxComponentValue = Mathf.Sqrt((maxForceValue * maxForceValue) / 3);
            x = UnityEngine.Random.Range(maxComponentValue/2, maxComponentValue);
            y = UnityEngine.Random.Range(maxComponentValue/2, maxComponentValue);
            z = UnityEngine.Random.Range(maxComponentValue/2, maxComponentValue);
            scaryObjects[i].transform.rigidbody.AddForce(new Vector3(x, y, z));
        }

        public void activateTrigger()
        {
            numTriggersActiveThisFrame++;
        }

        //possible num events: 
        //player walks into room, (trigger)
        //multiple triggers on top of each other
        //ghost activates room trigger
        //events would have to be triggered during the same update
        void Update()
        {
            //activateEvents(1);
            eventTimer += Time.deltaTime;

            if ( numTriggersActiveThisFrame > 0 && canEventActivate() )
            { 
                activateEvents(numTriggersActiveThisFrame);
            }

            numTriggersActiveThisFrame = 0;
        }

        //tries to throw as many objects as signals sent received this frame
        //avoids for throwing the same object twice
        //resets eventTimer
        public void activateEvents(int numEvents)
        {
            int numThrown = 0;
            for (int i = 0; i < numEvents; i++)
            {
                applyThrowForceToScaryObject(currObjectIndex);
                currObjectIndex++;
                if (currObjectIndex >= scaryObjects.Count())
                {
                    currObjectIndex = 0;
                }

                //only throw the number of objects in the room ( avoid double throwing)
                numThrown++;
                if (numThrown >= scaryObjects.Count())
                {
                    break;
                }
            }

            eventTimer = 0;
        }

        //checks if enough time has passed since last event
        public bool canEventActivate()
        {
            bool ret = false;
            if (eventTimer > timeLimit)
            {
                //We have waited long enough, and can fire an event
                ret = true;
            }
            return ret;
        }
    }
}
