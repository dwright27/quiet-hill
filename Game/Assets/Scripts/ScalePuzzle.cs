﻿using UnityEngine;
using System.Collections;
using System;

public class ScalePuzzle : MonoBehaviour {

    public GameObject stuffedScale;
    public GameObject emptyScale;
    public GameObject panel;

    private bool moveScale = false;
    private bool movePanel = false;
    private int bottleCount = 0;

    private float moveUnit = 0.0f;
    private float panelMovementDuration = 6.0f;
    private float panelMovementSpeed = 0.1f;
    private float panelMovementCounter = 0.0f;
    private float originalEmptyScaleY;
    private float originalStuffedScaleY;
    private float deltaY = 0.0f;
    private float delay = 2.0f;

    public int BottleCount
    {
        get { return bottleCount; }
    }

	void Start () {
        originalEmptyScaleY = emptyScale.transform.position.y;
        originalStuffedScaleY = stuffedScale.transform.position.y;
        deltaY = Math.Abs(originalEmptyScaleY - originalStuffedScaleY);
        moveUnit = deltaY * 0.5f / 3.0f;
	}
	
	// Update is called once per frame
	void Update () {
        MoveScale();
        CheckBalanced();
        MovePanel();
	}

    void CheckBalanced()
    {
        if (emptyScale.transform.position.y - stuffedScale.transform.position.y < 0.01f)
        {
            movePanel = true;   
        }
    }

    void MovePanel()
    {
        if (movePanel)
        {
            panelMovementCounter += Time.deltaTime;
        }

        if (movePanel && panelMovementCounter + delay < panelMovementDuration + delay)
        {
            panelMovementCounter += 0.1f;
            panel.transform.Translate(new Vector3(0.0f, 0.0f, panelMovementSpeed));
        }
    }

    void MoveScale()
    {
        if (emptyScale.transform.position.y > originalEmptyScaleY - bottleCount * moveUnit)
        {
            emptyScale.transform.Translate(.0f, -moveUnit * 0.1f, .0f);
        }

        if (stuffedScale.transform.position.y < originalStuffedScaleY + bottleCount * moveUnit)
        {
            stuffedScale.transform.Translate(.0f, moveUnit * 0.1f, .0f);
        }
        
    }

	public bool Solved()
	{
        return movePanel;//originalEmptyScaleY == originalStuffedScaleY;
	}

    public void BottleTaken()
    {
        bottleCount--;
    }

    public void BottleAdded()
    {
        bottleCount++;
    }
}
