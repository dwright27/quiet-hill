﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public class ScaryEventManager : MonoBehaviour
    {
        private Dictionary<ScaryEventType, float> events = 
            new Dictionary<ScaryEventType, float>();

        private IList<ScaryEventType> triggeredEvents = new List<ScaryEventType>();

        public float timeLimit = 180.0f;

        void Start ()
        {
            events.Add(ScaryEventType.DOOR_KNOCK, timeLimit);
            events.Add(ScaryEventType.DOOR_SLAMP, timeLimit);
            events.Add(ScaryEventType.FLOATING_OBJECT, timeLimit);
            events.Add(ScaryEventType.GHOST_SHOWING, timeLimit);
            events.Add(ScaryEventType.LITTLE_GIRL_SINGING, timeLimit);
            events.Add(ScaryEventType.RANDOM_SOUND, timeLimit);
            events.Add(ScaryEventType.SCREAM, timeLimit);
            events.Add(ScaryEventType.GHOST_SPAWNING,timeLimit);
        }

        void Update()
        { 
            foreach( var e in triggeredEvents)
            {
                events[e] += Time.deltaTime;
            }
        }

        public bool EventActivated(ScaryEventType type)
        {
            bool eventCanActivate = false;

            float lastTimeEventWasTriggered = 0.0f;
            events.TryGetValue(type, out lastTimeEventWasTriggered);

            eventCanActivate = lastTimeEventWasTriggered >= timeLimit;
           // eventCanActivate = true;
            if (eventCanActivate)
            {
                events[type] = 0.0f;
                triggeredEvents.Add(type);
            }

            return eventCanActivate;
        }

        public void EventEnded(ScaryEventType type)
        {
            events[type] = 0.0f;
            triggeredEvents.Remove(type);
        }
    }
}
