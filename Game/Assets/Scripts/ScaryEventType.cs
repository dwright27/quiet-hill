﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public enum ScaryEventType
    {
        DOOR_SLAMP,
        DOOR_KNOCK,
        FLOATING_OBJECT,
        LITTLE_GIRL_SINGING,
        SCREAM,
        RANDOM_SOUND,
        GHOST_SHOWING,
        GHOST_SPAWNING
    }
}
