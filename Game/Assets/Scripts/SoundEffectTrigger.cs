﻿using UnityEngine;
using System.Collections;

public class SoundEffectTrigger : BaseTrigger {


    public AudioClip[] soundEffects;
   // public AudioClip sound;
  //  public GameObject[] g;
    private bool startedMusic = false;



	// Use this for initialization
	void Start () {
        base.Start();
//        base.Init();
//        type = Assets.Scripts.ScaryEventType.GHOST_SPAWNING;
	}
	
	// Update is called once per frame
	void Update () {
	    if(startedMusic && !audio.isPlaying)
        {
            startedMusic = false;
            EventEnded();
        }
	}


    public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);
    }


    public override void Event()
    {
        audio.PlayOneShot(soundEffects[Random.Range(0, soundEffects.Length)]);
        startedMusic = true;
    }
}
