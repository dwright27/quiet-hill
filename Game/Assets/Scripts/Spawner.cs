﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject[] objectsToSpawn;
	public bool spawnOnce = true;
	private bool spawnedBefore = false;
	public void spawn()
	{
		if ( spawnOnce && spawnedBefore ) return;
		foreach(GameObject i in objectsToSpawn)
		{
			i.SetActive( true );
		}
		spawnedBefore = true;
	}
}
