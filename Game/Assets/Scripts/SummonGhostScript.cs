﻿using UnityEngine;
using System.Collections;

public class SummonGhostScript : MonoBehaviour {


    public bool ContactedPlayer
    {
        get;
        set;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.tag == "Player")
        {
            ContactedPlayer = true;
        }
    }


}
