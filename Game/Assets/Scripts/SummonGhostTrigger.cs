﻿using UnityEngine;
using System.Collections;

public class SummonGhostTrigger : BaseTrigger {

	// Use this for initialization
    public GameObject ghostPrefab;
    public float ghostSummonDistance = 30.0f;
    bool lerping = false;
    bool startLerp = false;
    private GameObject player;
    private Vector3 startPosition;
    private Vector3 endPosition;
    private float lerpPosition;
    private GameObject ghostInstance;
    public float speed = 5.0f;
    public float ghostHeight  = 10.0f;


	void Start () {
        base.Start();
        type = Assets.Scripts.ScaryEventType.GHOST_SPAWNING;
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
	    if(lerping && lerpPosition < 1.0f)
        {
            ghostInstance.transform.position = Vector3.Lerp(startPosition, endPosition, lerpPosition);
            lerpPosition += (Time.deltaTime * speed);
        }

        if(lerpPosition >= 1.0f)
        {
            lerping = false;
            lerpPosition = 0.0f;
            Destroy(ghostInstance); 
        }
	}

    public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);
    }

    public override void Event()
    {
        if (!lerping)
        {
            Vector3 forwardVector = Camera.main.transform.forward;


            float rotation = player.transform.rotation.eulerAngles.y;
            //startPosition = Quaternion.Euler(new Vector3(0, rotation - 45.0f, 0)) * player.transform.position + (forwardVector * ghostSummonDistance); // - new Vector3(ghostSummonDistance, 0, 0));
            startPosition =  player.transform.position + (forwardVector * ghostSummonDistance) - new Vector3(ghostSummonDistance, 0, 0);
            startPosition += Vector3.up * ghostHeight;

            //endPosition = Quaternion.Euler(new Vector3(0, rotation + 45.0f, 0)) * (player.transform.position + (forwardVector * ghostSummonDistance));// + new Vector3(ghostSummonDistance, 0f, 0));
            endPosition =  (player.transform.position + (forwardVector * ghostSummonDistance)) + new Vector3(ghostSummonDistance, 0f, 0);
            endPosition += Vector3.up * ghostHeight;

            //Camera.main.transform.rotation.eulerAngles.
            startLerp = true;
            lerping = true;
            lerpPosition = 0.0f;
            ghostInstance = (GameObject)Instantiate(ghostPrefab, startPosition, Quaternion.Euler(0.0f, rotation + 45.0f, 0.0f));
        }
    }
}
