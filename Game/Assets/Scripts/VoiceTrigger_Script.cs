﻿using UnityEngine;
using System.Collections;

public class VoiceTrigger_Script : MonoBehaviour {

    bool started = false;
	void Update () {
        if (started && !audio.isPlaying)
        {
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter(Collider collider)
    { 
        if(collider.tag == "Player" && !started)
        {
            audio.Play();
            started = true;
        }
    }
}
