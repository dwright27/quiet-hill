﻿using UnityEngine;
using System.Collections;

public class LightningScript : MonoBehaviour {
    //Each instance will produce multiple lightning bolt effect lights(strike lights), with a background ambient light effect
    // def: (strike light) thought of as each individual lightning bolt in the wave of bolts
    // def: (ambient light) the overarching background light produced by during the wave of lighting bolts

    //script should be attached to gameobject with two children named appropriately and with the following components:
        // 1) "ambientLight"
            // a) Light component (spot light) set with the appropriate ambient light color
        // 2) "strikeLight" 
            // a) Light component (directional) set with the appropriate strike light color 

    public float lightningDurationTime = 5; //duration of wave of lightning, also affects the duration of each lightning bolt (will change later)
    public uint numStrikes = 5; //indicates number of seperate flashes of light during lightning bolt strike
    public float maxAmbientIntensity = 1; //intensity ambient light of lightning strike will be raised to
    public float maxStrikeIntensity = 2; //intensity strike light of lighting will be raised to
    public float maxTimeBetweenInterval = 5;//time between the lighting bursts (random)
    public AudioClip[] thunderNoises;
   
    
    private uint noiseIndex = 0;
    private Light ambientLight;
    private Light strikeLight;
    private bool _lightningOn = false;
    private float genTimer = 0;
    private float lightningTimer = 0;
    private float currLightningSpacing = 0;
    private float timePerStrike;
    private float currStrikeTimer = 0;

    public bool lightningOn
    {
        get { return _lightningOn; }
        set { _lightningOn = !_lightningOn; }
    }
    
    void Start () {
        ambientLight = transform.Find("ambientLight").GetComponent<Light>();
        strikeLight = transform.Find("strikeLight").GetComponent<Light>();
        currLightningSpacing = Random.Range(0, maxTimeBetweenInterval);
        gameObject.audio.clip = thunderNoises[noiseIndex];
        gameObject.audio.PlayOneShot(audio.clip);
    }
	
	void Update () {
        if (_lightningOn)
        {
            updateAmbientLight();
            updateStrikeLight();
            updateLightningTimer();
        }
        else {
            //wait till next wave
            updateWaveInterval();
        }
    }

    private void updateWaveInterval()
    {
        genTimer += Time.deltaTime;
        if (genTimer > currLightningSpacing)
        {
            lightningStrike();
            genTimer = 0;
            //set next timeInterval
            currLightningSpacing = Random.Range(0, maxTimeBetweenInterval);
        }
    }

    public void lightningStrike()
    {
        _lightningOn = true;
        ambientLight.enabled = true;
        strikeLight.enabled = true;
        timePerStrike = lightningDurationTime / numStrikes;
        lightningTimer = 0;
        currStrikeTimer = 0;

        //gameObject.audio.Stop();
        noiseIndex++;
        if (noiseIndex > thunderNoises.Length-1)
            noiseIndex = 0;
        gameObject.audio.clip = thunderNoises[noiseIndex];
        gameObject.audio.Play();
        //gameObject.transform.parent;
    }

    //evenly space out strikes
    //randomly 
    private void updateStrikeLight()
    { 
        //turn on and off strike lights 
        //lerp quickly between 0 intensity and max intensity for 
        currStrikeTimer += Time.deltaTime;
        if (currStrikeTimer < timePerStrike/2)
        {
            //increase light
            strikeLight.intensity += maxStrikeIntensity * Time.deltaTime;
        }
        else if (currStrikeTimer < timePerStrike)
        {
           //decrease intensity
            strikeLight.intensity -= maxStrikeIntensity * Time.deltaTime;
        }
        else 
        {
            currStrikeTimer = 0;
        }
    }

    //Lerp from 0 to max intensity over duration
    private void updateAmbientLight()
    {
        if (lightningTimer < lightningDurationTime / 2)
        {
            //if you want to end up at a certain val then increment by every frame by the force* delta time
            ambientLight.intensity += maxAmbientIntensity * Time.deltaTime;
        }
        else
        {
            ambientLight.intensity -= maxAmbientIntensity * Time.deltaTime;
        }
    }

    //disable lights and reset timer
    private void endLightning()
    {
        _lightningOn = false;
        ambientLight.enabled = false;
        strikeLight.enabled = false;
    }

    //increments timer; calls endLighting() when past the duration time 
    private void updateLightningTimer()
    { 
        lightningTimer += Time.deltaTime;
        if (lightningTimer > lightningDurationTime)
        {
            endLightning();
        }
    }
}
