﻿using UnityEngine;
using System.Collections;


//attach this to the trigger which starts a scare
//or attach this to an investigatible 
//
public class introMusicStartScareTrigger_Script : MonoBehaviour
{
    public GameObject scareIntroMusicTrigger;
    private bool scareDone = false;

    public void Update()
    {
        if (!scareDone)
        {
            Component[] allComps = GetComponents<Component>();
            for(int i = 0; i < allComps.Length; i ++)
            {
                //this scareStartTrigger component is attached to an investigatable object which triggers a scary when investigated
                if (allComps[i] is RatInvestigate)
                {
                    if (((RatInvestigate)allComps[i]).wasinvestigated())
                    {
                        triggerInvestigateIntro();
                    }
                }
                else if (allComps[i] is Door)
                {
                    if (((Door)allComps[i]).isOpen())
                    {
                        triggerInvestigateIntro();
                    }
                }
            }
        }
    }

    public void triggerInvestigateIntro()
    {
        GetComponent<introMusicScareTrigger_Script>().triggerIntro();
        scareDone = true;
    }

    public void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player" && !scareDone)
        {
            scareIntroMusicTrigger.GetComponent<introMusicScareTrigger_Script>().PlayScareMusic();
        }
    }
}