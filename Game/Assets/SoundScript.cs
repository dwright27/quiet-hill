﻿using UnityEngine;
using System.Collections;

public class SoundScript : MonoBehaviour {


    public AudioClip sound;
    public float delay = 2.0f;
    public bool randomDelay = false;
    public float minDelay = 3.0f;
    public float maxDelay = 5.0f;

    private float timePassed = 0.0f;
    private float currentDelay;

	// Use this for initialization
	void Start () {
        currentDelay = (randomDelay) ? Random.Range(minDelay, maxDelay) : delay;
	}
	
	// Update is called once per frame
	void Update () {
        timePassed += Time.deltaTime;
	    if(timePassed > currentDelay)
        {
            timePassed = 0.0f;
            audio.PlayOneShot(sound);
            currentDelay = (randomDelay) ? Random.Range(minDelay, maxDelay) : delay;
        }
	}
}
