﻿using UnityEngine;
using System.Collections;

public class StopSinging : MonoBehaviour {

	// Use this for initialization

    public BedroomCutScene girl;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            girl.Sing = false;
        }
    }
}
