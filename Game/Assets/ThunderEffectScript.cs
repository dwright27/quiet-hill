﻿using UnityEngine;
using System.Collections;

public class ThunderEffectScript : MonoBehaviour {

    public AudioClip[] thunderNoises;
    private uint noiseIndex = 0;
    // Use this for initialization
	void Start () {
        gameObject.audio.clip = thunderNoises[noiseIndex];
    }

    public void playSound()
    {
        //if (checkIfCanPlaySound())
        //{
            audio.Stop();
            noiseIndex++;
            audio.clip = thunderNoises[noiseIndex];
            audio.Play();
        //}
    }

    private bool checkIfCanPlaySound()
    {
        return !audio.isPlaying;
    }
}