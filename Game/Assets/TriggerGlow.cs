﻿using UnityEngine;
using System.Collections;

public class TriggerGlow : MonoBehaviour {

	// Use this for initialization

    public Glow glow;
    public GameManagerScript gameManager;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(gameManager.isGhostOut())
        {
            glow.triggerGlow = true;
        }
        else
        {
            glow.triggerGlow = false;
        }
	}
}
