﻿using UnityEngine;
using System.Collections;

public class TriggerInvestigate : MonoBehaviour {

    public GameObject investigatable;
	// Use this for initialization
	void Start () {
        if (!investigatable)
        {
            Debug.LogError("Set the investigatable for this trigger");
        }
	}
	
    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            if (investigatable.GetComponent<Investigatable>() != null)
            {
                investigatable.GetComponent<Investigatable>().Investigate();
            }
            else {
                investigatable.GetComponentInChildren<Investigatable>().Investigate();
            }
        }
    }
}
