﻿using UnityEngine;
using System.Collections;

public class TurnNode : MonoBehaviour {

    public Vector3 rotationAxis;
    //tie the correct state to a certain side randomly
    private bool isActive;
    private int correctState;
    private int currState;
    private bool isTurning;
    private float currAngle;

    public int numStates;
    public float maxAngle;
    public float angleIncrementPerUpdate;

	void Start () {
        gameObject.audio.playOnAwake = true;
        isActive = true;
        correctState = Random.Range(0, numStates);
        currState = Random.Range(0, numStates);
        while (currState == correctState)
        {
            currState = Random.Range(0, numStates);
        }

        isTurning = false;
        currAngle = 0;
	}
	
    //turns the object
	void Update () {

        if (isTurning)
        {
            transform.Rotate(angleIncrementPerUpdate * rotationAxis);
            currAngle += angleIncrementPerUpdate;
            if (currAngle > maxAngle)
            {
                currAngle = 0;
                isTurning = false;
                if (audio.isPlaying)
                {
                    audio.Stop();
                }
            }
        }
	}

    //can only change state when isActive
    public void changeState()
    {
        if (!audio.clip.isReadyToPlay)
            audio.Stop();
        audio.Play();

        if (isActive)
        {
            if (!isTurning) { isTurning = true; }
            currState++;
            if (currState > numStates)
            {
                currState = 0;
            }
        }
    }

    //returns true if in the correct state
    public bool checkStatus() {
        return (currState == correctState);
    }

    public void setActiveState(bool state) {
        isActive = state;
    }
}