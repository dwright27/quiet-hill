﻿using UnityEngine;
using System.Collections;

public class WanderScript : MonoBehaviour
{


    public float speed = 20.0f;
    Vector3 wayPoint;
    public float range = 10.0f;

    // Use this for initialization
    void Start()
    {
        Wander();
    }

    private void Wander()
    {
        wayPoint = new Vector3(Random.Range(transform.position.x - range, transform.position.x + range), 3.0f, Random.Range(transform.position.z - range, transform.position.z + range));
        transform.LookAt(wayPoint);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.TransformDirection(Vector3.forward) * speed * Time.deltaTime;

        if ((transform.position - wayPoint).magnitude < 3)
        {
            Wander();
        }
    }



}
