﻿using UnityEngine;
using System.Collections;

/// scare can get triggered, but not started
/// triggered: intro music starts 
/// started: scare happens

//this component is attached to the trigger which starts the scare
//if scare is not started via trigger, then the intro music attached to this component can be triggered via a call to triggerIntro()
//
//when scareisIntroduced, the scare happens:
//1. if there is no scareStartTrigger attached, then scare happens after intro music finishes
    //unless there is a time limit set: then the music will 
//2. if there is a scareStartTrigger attached, the music will loop untill the scare is started
public class introMusicScareTrigger_Script : MonoBehaviour {

    //optional, if null then the scare will start immediately after the music stops
    public GameObject scareStartTrigger;
    public AudioClip introSound;
    //This sound is used to cover up the sudden stop of the looping intro music for Scares that use a second trigger to start the scare (you may want to start the intro music to a scare that the player cannot yet access)
    //this is supposed to be played at the moment of impact of the scare
    public AudioClip scareMomentSound;
    public bool startScareAfterTimeLimit = false;
    public float timeLimit = 5.0f;

    private float musicTimer = 0;
    private bool introMusicStarted = false;

    //need to attach this to investigatable objects
    //when the player investigates, call the trigger intro()

    //start scare trigger will call the start scare method when trigger is entered
    //need to set the audio looping
    void Update()
    {
        if (!scareStartTrigger && introMusicStarted )
        {
            musicTimer += Time.deltaTime;
            if (startScareAfterTimeLimit && musicTimer > timeLimit || !startScareAfterTimeLimit && !audio.isPlaying)
            {
                PlayScareMusic();
            }
        }
    }

    //Player triggers the intro music to the scare
    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            triggerIntro();
        }
    }

    //starts intro music
    public void triggerIntro()
    {
        audio.clip = introSound;
        if (scareStartTrigger)
        {
            audio.loop = true;
        }
        audio.Play();
        introMusicStarted = true;
    }

    //starts the actual scary part of the scare
    public void PlayScareMusic()
    {
        if (audio.loop)
        {
            audio.loop = false;
            audio.Stop();
            audio.clip = scareMomentSound;
            audio.Play();
        }
    }
}